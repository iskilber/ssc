import {
  AnyEvent,
  APP_ENV_TOKEN,
  ApplicationState,
  ConfigurationService,
  EventBus,
  EventsObservable,
  FluxStandardAction,
  HttpClient
  } from '@ssc/sdk';
import * as React from 'react';
import {
  MonoTypeOperatorFunction,
  Observable,
  Subject,
  Subscription
  } from 'rxjs';
import {
  filter,
  share,
  takeUntil
  } from 'rxjs/operators';
import { useInject } from '../injector';

export function useEventBus(): EventBus {
  const { eventBus } = useInject({ eventBus: EventBus });

  return eventBus;
}

export function useDispatch() {
  const eventBus = useEventBus();

  return React.useCallback((action: FluxStandardAction<any, any, any>) => {
    eventBus.dispatch(action);
  }, [eventBus]);
}

export function useHttpClient(): HttpClient {
  const { http } = useInject({ http: HttpClient });

  return http;
}

export function useConfig(): ConfigurationService {
  const { config } = useInject({ config: ConfigurationService });

  return config;
}

export function useAppEnv(): string {
  const { appEnv } = useInject({ appEnv: APP_ENV_TOKEN });

  return appEnv;
}

export function useExposeAppState(debugName: string) {
  const appEnv = useAppEnv();
  if (appEnv !== 'dev') { return; 
  }
  const { appState } = useInject({ appState: ApplicationState });
  
  if (!window[debugName]) {
    window[debugName] = appState;
  }
}

export function useEventLogging() {
  const eventBus = useEventBus();
  const appEnv = useAppEnv();

  React.useEffect(() => {
    const logStyles = [
      `background: url("${location.href}img/console-icon-redux.png") no-repeat`,
      'font-weight: bold',
      'padding-left: 16px',
      'margin-left: 2px',
    ];
    const subscription = eventBus
        .addEventHandler((events$) => events$
            .pipe(filter(() => appEnv === 'dev'))
            .subscribe((event) => console.info(
                `%c[${event.type}]:`, logStyles.join(';'), event.payload || '')));

    return () => subscription.unsubscribe();
  }, [eventBus, appEnv]);
}

export function useSubscription(subscribe: () => Subscription, deps: any[]) {
  React.useEffect(() => {
    const subscription = subscribe();

    return () => subscription.unsubscribe();
  }, deps);
}

export function useRefByCallback<T>(
  callback: () => T
): React.MutableRefObject<T | undefined> {
  const ref = React.useRef<T>();

  if (!ref.current) {
    ref.current = callback();
  }
  return ref;
}

export function useEventHandler<T extends AnyEvent>(
  handler: (events$: Observable<T>) => Subscription,
  deps: any[] = []
) {
  const eventBus = useEventBus();

  useSubscription(
    () => eventBus.addEventHandler(handler),
    [eventBus, ...deps]);
}

export function useEventMiddleware<T extends AnyEvent>(
  middleware: (events$: EventsObservable<T>) => Observable<any>,
  deps: any[]
) {
  const eventBus = useEventBus();

  React.useEffect(() => {
    const stopper$ = new Subject();

    eventBus.addEventMiddleware(
        (events$) => middleware(events$).pipe(takeUntil(stopper$)));

    return () => stopper$.next();
  }, [...deps, eventBus]);
}

class StopperController {
  private stopper$ = new Subject<void>();
  public sharedStopper$ = this.stopper$.pipe(share());

  public stop() {
    this.stopper$.next();
  }

  public takeUntil<T>(): MonoTypeOperatorFunction<T> {
    return takeUntil(this.sharedStopper$);
  }
}

export function useStopper() {
  const stopper = useRefByCallback<StopperController>(() => new StopperController());

  React.useEffect(() => {
    stopper.current.stop();
  }, [stopper]);

  return stopper.current;
}