import {
  actionOf,
  ReflectionHelper
  } from '@ssc/sdk';
import * as React from 'react';
import {
  FormattedMessage,
  InjectedIntl
  } from 'react-intl';
import { map } from 'rxjs/operators';
import { useEventHandler } from '../customs';
import { useInject } from '../injector';
import { IntlMessage } from './intl-message';
import {
  I18N_MESSAGES_TOKEN,
  IntlContext
  } from './intl.context';
import { IntlSwitchAction } from './intl.events';
import { useIntlStore } from './intl.injector';

export function useIntl(): InjectedIntl | null {
  return React.useContext(IntlContext);
}

export function useTranslate(message: IntlMessage.Message): string {
  const intl = useIntl();

  return React.useMemo(() => {
    return ReflectionHelper.isString(message) ? 
        message as string:
        intl!.formatMessage(message as FormattedMessage.Props);
  }, [message]);
}

export function useTranslateClb(): (message: IntlMessage.Message) => string {
  const intl = useIntl();

  return React.useCallback((message: IntlMessage.Message) => {
    return ReflectionHelper.isString(message) ?
        message as string :
        intl!.formatMessage(message as FormattedMessage.Props)
  }, [intl]);
}

export function useI18NMessages() {
  const { messages } = useInject({ messages: I18N_MESSAGES_TOKEN });

  return messages;
}

export function useIntlFeature() {
  const intlStore = useIntlStore();

  useEventHandler((actions$) => actions$
      .pipe(
          actionOf(IntlSwitchAction),
          map((action: IntlSwitchAction.Action) => action.payload.locale))
      .subscribe((locale) => intlStore.setLocale(locale)),
    [])
}