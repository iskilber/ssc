import {
  ApplicationState,
  ObservableMapStore,
  ObservableStore
  } from '@ssc/sdk';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';

export declare namespace IntlStore {
  interface Messages {
    [key: string]: string
  }

  interface State {
    messages: { [locale: string]: Messages }
    currentLocale: string;
    defaultLocale: string;
    enabledLocales: string[];
  }
}

export class IntlStore extends ObservableStore<IntlStore.State> {
  constructor(
    appState: ApplicationState, 
    initState: Partial<IntlStore.State>
  ) {
    super('app.dictionary', appState, {
      messages: {},
      ...initState,
    } as IntlStore.State);
  }

  public getDefaultLocale(): string {
    return this.getState().defaultLocale;
  }

  public getCurrentLocale(): string {
    return this.getState().currentLocale;
  }

  public getEnabledLocales(): string[] {
    return this.getState().enabledLocales;
  }

  public getMessages(locale: string): IntlStore.Messages {
    return this.getState().messages[locale] || {};
  }

  public getCurrentMessages(): IntlStore.Messages {
    return this.getMessages(this.getCurrentLocale());
  }

  public setLocale(locale: string) {
    const prevState = this.getState();
    const newState = { ...prevState, currentLocale: locale };

    this.setState(newState);
  }

  public observeLocale(): Observable<string> {
    return this.change.pipe(
        filter((change) => 
            change.prevState.currentLocale !== change.nextState.currentLocale),
        map((change) => change.nextState.currentLocale));
  }
}
