import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { useIntlStore } from './intl.injector';
import { IntlStore } from './intl.store';

const IntlContext = React.createContext<
  IntlDynamicProvider.IntlController | null
>(null);

export declare namespace IntlDynamicProvider {
  interface IntlController {
    switchLanguage: (languageCode: string) => void;
  }

  type Component = React.FunctionComponent;
}

export const DynamicIntlConsumer = IntlContext.Consumer;

export const IntlDynamicProvider: IntlDynamicProvider.Component = (props) => {
  const intlStore = useIntlStore();

  const [locale, setLocale] = React.useState<string>(intlStore.getCurrentLocale());
  const switchLanguage = React.useCallback((locale: string) => {
    intlStore.setLocale(locale);
  }, [intlStore]);

  React.useEffect(() => {
    const subscription = intlStore
        .observeLocale()
        .subscribe((locale) => setLocale(locale));

    return () => subscription.unsubscribe();
  }, [intlStore, setLocale]);

  return (
    <IntlContext.Provider value={{ switchLanguage }}>
      <IntlProvider
          defaultLocale={intlStore.getDefaultLocale()}
          locale={locale}
          key={locale}
          messages={intlStore.getCurrentMessages()}>
        {props.children}
      </IntlProvider>
    </IntlContext.Provider>
  );
}

IntlDynamicProvider.displayName = 'IntlDynamicProvider';
