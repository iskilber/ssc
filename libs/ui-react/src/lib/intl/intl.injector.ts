import { useInject } from '../injector';
import { IntlStore } from './intl.store';

export const INTL_LANGUAGES_TOKEN = Symbol('app.intl.languages');

export function useIntlStore(): IntlStore {
  const { store } = useInject({ store: IntlStore });

  return store;
}
