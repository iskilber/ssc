import { FluxStandardAction } from '@ssc/sdk';

export enum INTL_EVENT_TYPES {
  SWITCH = 'ui.intl.switch'
}

/**
 * Ask console output feature to print a message on screen.
 */
export declare namespace IntlSwitchAction {
  type ACTION_TYPE = INTL_EVENT_TYPES.SWITCH;
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
  interface Payload {
    locale: string;
  }
}

export class IntlSwitchAction {
  public static ACTION_TYPE: INTL_EVENT_TYPES.SWITCH = 
    INTL_EVENT_TYPES.SWITCH;

  public static create(
    payload: IntlSwitchAction.Payload
  ): IntlSwitchAction.Action {
    return FluxStandardAction.create(IntlSwitchAction.ACTION_TYPE, payload);
  }
}