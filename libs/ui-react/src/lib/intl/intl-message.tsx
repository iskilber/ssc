import { ReflectionHelper } from '@ssc/sdk';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

export declare namespace IntlMessage {
  type Message = string | FormattedMessage.Props;

  interface Props {
    message: Message;
  }

  type Component = React.FunctionComponent<Props>;
}

export const IntlMessage: IntlMessage.Component = (props) => {
  if (ReflectionHelper.isString(props.message)) {
    return (<span>{props.message}</span>);
  } else {
    return (<FormattedMessage {...props.message as FormattedMessage.Props} />);
  }
};

IntlMessage.displayName = 'IntlMessage';
