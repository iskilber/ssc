import {
  Injector,
  Provider
  } from '@ssc/sdk';
import * as React from 'react';
import { InjectorContext } from './injector.context';

export declare namespace InjectorProvider {
  interface Props {
    providers?: Array<Provider<any>>;
  }

  type Component = React.StatelessComponent<Props>;
}

export const InjectorProvider: InjectorProvider.Component = (props) => {
  const parent = React.useContext(InjectorContext);
  const injector = React.useRef<Injector | undefined>(undefined);

  if (!injector.current) {
    injector.current = new Injector(parent || undefined);
    if (props.providers) {
      injector.current.use(props.providers);
    }
  }

  return (
    <InjectorContext.Provider value={injector.current}>
      {props.children}
    </InjectorContext.Provider>
  );
}