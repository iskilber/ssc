import { Injector } from '@ssc/sdk';
import * as React from 'react';

export const InjectorContext: React.Context<Injector | undefined> =
  React.createContext<Injector | undefined>(undefined); 

InjectorContext.displayName = 'InjectorContext';