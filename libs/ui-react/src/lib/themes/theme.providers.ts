import {
  actionOf,
  ApplicationState,
  EventBus,
  FactoryProvider
  } from '@ssc/sdk';
import { ThemeChangeAction } from './theme.events';
import { ThemeStore } from './theme.store';

export const UI_THEME_INIT_TOKEN = Symbol('app.ui.theme.init');

const themeStoreProvider = new FactoryProvider(
  ThemeStore,
  (eventBus: EventBus, appState: ApplicationState, init: ThemeStore.State) => {
    const store = new ThemeStore(appState, init);

    eventBus.addEventHandler((events$) => events$
      .pipe(actionOf(ThemeChangeAction))
      .subscribe((action: ThemeChangeAction.Action) => {
        switch (action.type) {
          case ThemeChangeAction.ACTION_TYPE:
            store.setCurrentTheme(action.payload!.themeName);
            break;
        }
      }));

    return store;
  },
  [EventBus, ApplicationState, UI_THEME_INIT_TOKEN]
);

export const UI_THEME_PROVIDERS = [
  themeStoreProvider
];
