import { FluxStandardAction } from '@ssc/sdk';
import { string } from 'prop-types';

export enum THEME_EVENT_TYPES {
  CHANGE = 'ui.themes.change'
}


/**
 * Configuration data has been loaded into app
 */
export declare namespace ThemeChangeAction {
  type ACTION_TYPE = THEME_EVENT_TYPES.CHANGE;
  interface Payload {
    themeName: string;
  }
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
}

export class ThemeChangeAction {
  public static ACTION_TYPE: THEME_EVENT_TYPES.CHANGE = 
    THEME_EVENT_TYPES.CHANGE;

  public static create(
    payload: ThemeChangeAction.Payload
  ): ThemeChangeAction.Action {
    return FluxStandardAction.create(ThemeChangeAction.ACTION_TYPE, payload);
  }
}