import {
  ApplicationState,
  ObservableStore
  } from '@ssc/sdk';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';

export declare namespace ThemeStore {
  interface State {
    currentThemeName: string;
    defaultThemeName: string;
    themeNames: string[];
  }

  interface ThemeChange { 
    prevTheme: string;
    nextTheme: string;
  }
}

export class ThemeStore extends ObservableStore<ThemeStore.State> {
  constructor(appState: ApplicationState, init: ThemeStore.State) {
    super('ui.theme', appState, init);
  }

  public observeCurrentTheme(): Observable<ThemeStore.ThemeChange> {
    return this.change.pipe(
        filter((change) => change.prevState.currentThemeName !== change.nextState.currentThemeName),
        map((change) => ({
          prevTheme: change.prevState.currentThemeName,
          nextTheme: change.nextState.currentThemeName
        })));
  }

  public setCurrentTheme(themeName: string) {
    const prevState = this.getState();
    const newState = {
      ...prevState,
      currentThemeName: themeName
    };
    this.setState(newState);
  }

  public getCurrentThemeName(): string | undefined {
    return this.getState().currentThemeName;
  }

  public getThemeNames(): string[] {
    return this.getState().themeNames || [];
  }
}
