import { actionOf } from '@ssc/sdk';
import * as React from 'react';
import { map } from 'rxjs/operators';
import {
  useEventBus,
  useEventHandler
  } from '../customs';
import { useInject } from '../injector';
import { ThemeChangeAction } from './theme.events';
import { ThemeStore } from './theme.store';

export function useThemeStore(): ThemeStore {
  const { themeStore } = useInject({ themeStore: ThemeStore });

  return themeStore;
}

export function useThemeSwitchObserver() {
  const themeStore = useThemeStore();

  React.useEffect(() => {
    const currentClassName = themeStore.getCurrentThemeName();

    document.body.classList.add(currentClassName);

    const subscription = themeStore.observeCurrentTheme().subscribe((themeName) => {
      const nextClassName = themeName.nextTheme;
      const prevClassName = themeName.prevTheme;

      if (prevClassName) {
        document.body.classList.remove(prevClassName);
      }
      if (nextClassName) {
        document.body.classList.add(nextClassName);
      }
    });

    return () => subscription.unsubscribe();
  }, [themeStore]);
}

export function useThemeSwitch() {
  const eventBus = useEventBus();

  return React.useCallback((themeName: string) => {
    eventBus.dispatch(ThemeChangeAction.create({ themeName }));
  }, [eventBus]);
}

export function useThemeFeature() {
  const themeStore = useThemeStore();

  useEventHandler(
    (actions$) => actions$
      .pipe(
        actionOf(ThemeChangeAction),
        map((action: ThemeChangeAction.Action) => action.payload.themeName))
      .subscribe((themeName) => themeStore.setCurrentTheme(themeName)), 
    []);
}