import {
  APP_ENV_TOKEN,
  FactoryProvider,
  ValueProvider
  } from '../di';
import { EventBus } from '../events';
import { HttpClient } from '../http';
import { ApplicationState } from '../store';
import { ConfigurationService } from './configuration.service';
import { ConfigurationStore } from './configuration.store';

export const APP_CONFIG_DEFAULT = Symbol('app.config.default');

// Store here config compiled settings
export const APP_CONFIG_ENV_VALUES = Symbol('app.config.env.values');

// Feel free to override in any main app providers.
const appDefaultConfig = new ValueProvider(APP_CONFIG_DEFAULT, {});

const configStoreProvider = new FactoryProvider(
  ConfigurationStore,
  (appState: ApplicationState, appDefaultConfig: Partial<ConfigurationStore.State>) => 
      new ConfigurationStore(appState, appDefaultConfig),
  [ApplicationState, APP_CONFIG_DEFAULT]
);

const configServiceProvider = new FactoryProvider(
  ConfigurationService,
  (http: HttpClient, appEnv: string, eventBus: EventBus, store: ConfigurationStore) => {
    const service = new ConfigurationService(http, appEnv, eventBus, store)

    eventBus.addEventHandler(service.listen);

    return service;
  },
  [ HttpClient, APP_ENV_TOKEN, EventBus, ConfigurationStore ]
);

export const SDK_CONFIG_PROVIDERS = [
  appDefaultConfig,
  configStoreProvider,
  configServiceProvider
]