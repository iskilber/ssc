import { Errno } from '../errors';

export const ERR_CONFIG_MISSING_VALUE: Errno = {
  code: 'ERR_CONFIG_001',
  errno: 'ERR_CONFIG_MISSING_VALUE',
  message: 'Missing required configuration value for "{key}"'
}