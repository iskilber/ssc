import { FluxStandardAction } from '../events';
import { ConfigurationStore } from './configuration.store';

export enum CONFIGURATION_EVENT_TYPES {
  DATA = 'sdk.config.data',
  LOAD_END = 'sdk.config.load.end',
}

/**
 * Configuration data has been loaded into app
 */
export declare namespace ConfigurationDataAction {
  type ACTION_TYPE = CONFIGURATION_EVENT_TYPES.DATA;
  type Payload = ConfigurationStore.State;
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
}

export class ConfigurationDataAction {
  public static ACTION_TYPE: CONFIGURATION_EVENT_TYPES.DATA = 
      CONFIGURATION_EVENT_TYPES.DATA;

  public static create(
    payload: ConfigurationStore.State
  ): ConfigurationDataAction.Action {
    return FluxStandardAction.create(ConfigurationDataAction.ACTION_TYPE, payload);
  }
}

/**
 * Configuration loading process done.
 */
export declare namespace ConfigurationLoadeEndAction {
  type ACTION_TYPE = CONFIGURATION_EVENT_TYPES.LOAD_END;
  type Action = FluxStandardAction<ACTION_TYPE>;
}

export class ConfigurationLoadeEndAction {
  public static ACTION_TYPE: CONFIGURATION_EVENT_TYPES.LOAD_END = 
      CONFIGURATION_EVENT_TYPES.LOAD_END;

  public static create(): ConfigurationLoadeEndAction.Action {
    return FluxStandardAction.create(ConfigurationLoadeEndAction.ACTION_TYPE);
  }
}