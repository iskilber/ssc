import { ErrnoError } from '../errors';
import {
  ApplicationState,
  ObservableStore
  } from '../store';
import { ERR_CONFIG_MISSING_VALUE } from './configuration.errors';

export declare namespace ConfigurationStore {
  interface State {
    [key: string]: any
  }
}

export class ConfigurationStore extends ObservableStore<ConfigurationStore.State> {
  constructor(
    appState: ApplicationState, 
    defaultAppState: Partial<ConfigurationStore.State> = {}
  ) {
    super('app.config', appState, defaultAppState);
  }

  public merge(data: Partial<ConfigurationStore.State>) {
    const state = this.getState();

    // Put some more complex merging logic here.
    const newState = {
      ...state,
      ...data
    };

    this.setState(newState);
  }

  public get<T>(key: string, isOptional = false) {
    const state = this.getState();

    if (!Reflect.has(state, key) && !isOptional) {
      throw new ErrnoError(ERR_CONFIG_MISSING_VALUE, { key });
    }

    return Reflect.get(state, key) as T;
  }
}