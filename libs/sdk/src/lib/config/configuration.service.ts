import {
  actionOf,
  EventBus,
  EventsObservable
  } from '../events';
import { HttpClient } from '../http';
import {
  ConfigurationDataAction,
  ConfigurationLoadeEndAction
  } from './configuration.events';
import { ConfigurationStore } from './configuration.store';

export class ConfigurationService {
  public constructor(
    private http: HttpClient,
    private appEnv: string,
    private eventBus: EventBus,
    private configStore: ConfigurationStore
  ) {}

  public load(configUrl: string | string[]) {
    const configUrls = Array.isArray(configUrl) ? configUrl : [configUrl];
    
    return Promise.all(configUrls
        .map(this.buildUrl)
        .map((configUrl) => this.http.get(configUrl).toPromise()
            .then((config) => this.eventBus.dispatch(ConfigurationDataAction.create(config)))))
      .then(() => this.eventBus.dispatch(ConfigurationLoadeEndAction.create()));
  }

  public listen = (events$: EventsObservable<ConfigurationDataAction.Action>) => {
    return events$
      .pipe(actionOf(ConfigurationDataAction))
      .subscribe(this.reduce);
  }

  public reduce = (action: ConfigurationDataAction.Action) => {
    switch (action.type) {
      case ConfigurationDataAction.ACTION_TYPE:
        this.configStore.merge(action.payload!);
        break;
    }
  }

  private buildUrl = (configUrlTemplate: string) => {
    return configUrlTemplate.replace('{env}', this.appEnv);
  }
}