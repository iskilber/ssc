export enum HttpHeader {
  /* tslint:disable max-line-length */
  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept */
  ACCEPT         = 'Accept',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization */
  AUTHORIZATION  = 'Authorization',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-control */
  CACHE_CONTROL  = 'cache-control',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Length */
  CONTENT_LENGTH = 'content-length',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type */
  CONTENT_TYPE   = 'content-type',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Pragma */
  PRAGMA         = 'pragma',
  /* tslint:enable */
}
