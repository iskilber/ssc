/**
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
 */
export enum HttpMethod {
  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/CONNECT */
  CONNECT = 'CONNECT',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE */
  DELETE  = 'DELETE',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD */
  HEAD    = 'HEAD',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET */
  GET     = 'GET',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS */
  OPTIONS = 'OPTIONS',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH */
  PATCH   = 'PATCH',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST */
  POST    = 'POST',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT */
  PUT     = 'PUT',

  /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/TRACE */
  TRACE   = 'TRACE',
}
