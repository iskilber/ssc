import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ErrnoError } from '../../errors';
import {
  HttpHeader,
  HttpMethod,
  HttpStatusCode
  } from '../common';
import { ERR_HTTP_REQUEST_INVALID_CONTENT_TYPE } from './errors';
import { HttpClient } from './http-client';
import { HttpParams } from './http-params';
import { HttpUrl } from './http-url';
import { HttpInterceptor } from './http.interceptor';

export declare namespace HttpRequest {

  interface Init<D = any> extends RequestInit {
    data: D;
    method?: HttpMethod;
    base: string;
    params?: HttpParams.Init | HttpParams;
    parseRequest?: DataParser;
  }

  type Body = 
    Blob | Int8Array | Int16Array | Int32Array | Uint8Array |
    Uint16Array | Uint32Array | Uint8ClampedArray | Float32Array |
    Float64Array | DataView | ArrayBuffer | FormData | string | null;

  type DataParser = (data: any) => Body;
}

export class HttpRequest<D = any> {

  public static defaultsOpts = {
    acceptableStatuses: 
        (status: HttpStatusCode) => status < HttpStatusCode.BAD_REQUEST,
    retryAttempts: 0,
    method: HttpMethod.GET,
  };

  public readonly url: HttpUrl;

  private requestInit: HttpRequest.Init<D>;

  constructor(
    private partialUrl: string, 
    requestInit: HttpRequest.Init<D>
  ) {
    this.url = new HttpUrl({
      base: requestInit.base,
      params: requestInit.params,
      url: partialUrl,
    });

    this.requestInit = {
      ...HttpRequest.defaultsOpts,
      ...requestInit,
      headers: requestInit.headers instanceof Headers ? 
          requestInit.headers : 
          new Headers(requestInit.headers || {}),
    };
  }

  public get headers(): Headers {
    return this.requestInit.headers as Headers;
  }

  public get method(): HttpMethod {
    return this.requestInit.method as HttpMethod;
  }

  public get base(): string {
    return this.requestInit.base;
  }

  public get data(): D | undefined {
    return this.requestInit.data;
  }

  public get params(): HttpParams.Init | HttpParams | undefined {
    return this.requestInit.params;
  }

  public get parseRequest(): HttpRequest.DataParser | undefined {
    return this.requestInit.parseRequest;
  }

  public toRequest(): Request {
    return new Request(this.url.toString(), this.requestInit);
  }

  public clone(requestInit: Partial<HttpRequest.Init<D>> = {}) {
    return new HttpRequest(this.partialUrl, {
      ...this.requestInit,
      ...requestInit,
    } as HttpRequest.Init<D>);
  }


  /**
   * For request content type: application/json
   */
  public static parseJSONData(data: any = {}) {
    return JSON.stringify(data);
  }

  /**
   * For request content type application/x-www-form-urlencoded
   */
  public static parseXwwwFormUrlEncodedData(data: any = {}) {
    return Reflect.ownKeys(data)
      .map((key: string) =>
          [].concat(data[key])
            .map((value) =>
                `${encodeURIComponent(key)}=` +
                `${encodeURIComponent(value)}`)
            .join('&'))
      .join('&');
  }

  public static getRequestHeaderDecorator(
    headerName: string, headerValue: string, forceOverwrite: boolean = false
  ): HttpInterceptor.RequestInterceptor {
    return (request$) => request$.pipe(map((request) => {
      const headers = new Headers(request.headers);
      if (!headers.has(headerName) || forceOverwrite) {
        headers.set(headerName, headerValue);
      }

      return request.clone({ headers });
    }));
  }

  public static getRequestUuidDecorator(
    uuidParamName: string,
    createUuid: () => string
  ): HttpInterceptor.RequestInterceptor {
    return (request$: Observable<HttpRequest>): Observable<HttpRequest> => {
      return request$.pipe(map((request) => {
        const params = request.url.params.clone();
        params.set(uuidParamName, createUuid());
  
        return request.clone({ params });
      }));
    };
  }

  public static getRequestDataParser(
    parsers: {[contentType: string]: HttpRequest.DataParser}
  ): HttpInterceptor.RequestInterceptor {
    return (request$: Observable<HttpRequest>, init: HttpClient.FetchInit) => {
      return request$.pipe(map((request: HttpRequest): HttpRequest => {
        // Request with GET/HEAD method cannot have body
        if ([HttpMethod.GET, HttpMethod.HEAD].includes(request.method)) {
          return request;
        }
        const data = request.data;
        const contentType = request.headers.get(HttpHeader.CONTENT_TYPE);

        let body: any;
        if (init.parseRequest) {
          body = request.clone({ body: init.parseRequest(data)});
        } else if (!contentType) {
          throw new ErrnoError(ERR_HTTP_REQUEST_INVALID_CONTENT_TYPE);
        } else if (Reflect.has(parsers, contentType!)) {
          body = parsers[contentType](data)
        } else {
          body = data;
        }
    
        return request.clone({ body });
      }));
    }  
  }
}
