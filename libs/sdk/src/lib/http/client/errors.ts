import { Errno } from '../../errors';

export const ERR_HTTP_FETCH_INVALID_BASE: Errno = {
  code: 'HTTP-0001',
  errno: 'ERR_HTTP_FETCH_INVALID_BASE',
  message: 'Invalid or missing url base. Set correct base by ' +
      'using "addFetchDefaults"',
};

export const ERR_HTTP_REQUEST_INVALID_CONTENT_TYPE: Errno = {
  code: 'HTTP-0002',
  errno: 'ERR_HTTP_REQUEST_INVALID_CONTENT_TYPE',
  message: 'Invalid or missing request content type. Set content type ' +
      'type header via interceptor or in request init.',
};
