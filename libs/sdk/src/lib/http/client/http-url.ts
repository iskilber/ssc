import { HttpParams } from './http-params';

export interface HttpUrlInit {
  base?: string | URL;
  url: string;
  params?: HttpParams.Init | HttpParams;
}

export class HttpUrl extends URL {

  private static buildUrl(
    url: string,
    params?: HttpParams.Init | HttpParams
  ) {
    // Examin url string only if there is any param placeholder.
    const _params = params instanceof HttpParams ?
        params.toObject() : params || {};
    const {parsedUrl, searchParams} = Object.keys(_params || {})
        .reduce(
          (acc, paramName) => {
            const reqExp = new RegExp(`:${paramName}`);
            if (reqExp.test(acc.parsedUrl)) {
              return {
                ...acc,
                parsedUrl: acc.parsedUrl.replace(
                    `:${paramName}`, '' + _params[paramName]),
              };
            } else {
              acc.searchParams.append(paramName, '' + _params[paramName]);
              return acc;
            }
          },
          { parsedUrl: url, searchParams: new URLSearchParams() });
    const searchParamString = searchParams.toString();

    return `${parsedUrl}${searchParamString ? `?${searchParamString}` : ''}`;
  }

  public readonly params: HttpParams;

  public readonly base?: string | URL;

  private _url: string;

  constructor(init: HttpUrlInit) {
    super(HttpUrl.buildUrl(init.url, init.params), init.base);

    this.params = new HttpParams(init.params || {});

    this._url = init.url;

    this.base = init.base;
  }

  public clone(init: Partial<HttpUrlInit> = {}): HttpUrl {
    return new HttpUrl({
      base: init.base || this.base,
      params: init.params || this.params,
      url: init.url || this._url,
    });
  }
}
