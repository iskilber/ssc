import {
  Observable,
  of
  } from 'rxjs';
import {
  map,
  mergeMap
  } from 'rxjs/operators';
import {
  HttpMethod,
  HttpStatusCode
  } from '../common';
import { HttpRequest } from './http-request';
import { HttpResponse } from './http-response';
import { HttpInterceptor } from './http.interceptor';

export declare namespace HttpClient {
  interface Init {
    acceptableStatuses?: HttpResponse.AcceptableStatuses,
    retryAttempts?: number;
    retryStrategy?: RetryStrategyFn;
    requestParsers?: { [contentType: string]: HttpRequest.DataParser };
    responseParsers?: { [contentType: string]: HttpResponse.BodyParser };
    /**
     * Response interceptors are invoked before asserting response status
     * or before retry attepts.
     */
    responseInterceptors?: 
        HttpInterceptor.Interceptor<HttpResponse<any>> |
        Array<HttpInterceptor.Interceptor<HttpResponse<any>>>;

    /**
     * Request interceptors are invoked before parsing request data to body.
     */
    requestInterceptors?: 
        HttpInterceptor.Interceptor<HttpRequest> |
        Array<HttpInterceptor.Interceptor<HttpRequest>>;

    base?: string;
  }

  type FetchInit<D = any, DD = any> = 
      HttpRequest.Init<D> & HttpResponse.Init<DD> & Init;

  type DefaultInit<D = any, DD = any> = Partial<FetchInit<D, DD>>;

  type RetryStrategyFn = (errors$: Observable<any>) => Observable<any>;
}

export class HttpClient {

  private defaultInit: HttpClient. DefaultInit = {};

  public requestParser = new HttpInterceptor<HttpRequest>();
  public responseParser = new HttpInterceptor<HttpResponse<any>>();
  public requestInterceptors = new HttpInterceptor<HttpRequest>();
  public responseInterceptors = new HttpInterceptor<HttpResponse<any>>();

  public addFetchDefaults(opts: HttpClient. DefaultInit): this {
    this.defaultInit = { ...this.defaultInit, ...opts };
    return this;
  }

  public get<D>(
    url: string,
    fetchInit: HttpClient. DefaultInit = {}
  ): Observable<D> {
    return this.fetch<D>(url, { ...fetchInit, method: HttpMethod.GET });
  }

  public post<D>(
    url: string,
    fetchInit: HttpClient. DefaultInit = {}
  ): Observable<D> {
    return this.fetch<D>(url, { ...fetchInit, method: HttpMethod.POST });
  }

  public put<D>(
    url: string,
    fetchInit: HttpClient. DefaultInit = {}
  ): Observable<D> {
    return this.fetch<D>(url, { ...fetchInit, method: HttpMethod.PUT });
  }

  public delete<D>(
    url: string,
    fetchInit: HttpClient. DefaultInit = {}
  ): Observable<D> {
    return this.fetch<D>(url, { ...fetchInit, method: HttpMethod.DELETE });
  }

  public connect<D>(
    url: string,
    fetchInit: HttpClient. DefaultInit = {}
  ): Observable<D> {
    return this.fetch<D>(url, { ...fetchInit, method: HttpMethod.CONNECT });
  }

  public fetch<D>(
    url: string,
    fetchInit: HttpClient. DefaultInit = {}
  ): Observable<D> {
    const init = { ...this.defaultInit, ...fetchInit } as HttpClient.FetchInit;
    const httpRequest = new HttpRequest(
        url, 
        { ...this.defaultInit, ...fetchInit } as HttpRequest.Init);

    return of(httpRequest).pipe(
      this.requestInterceptors.intercept<D>(init),
      this.requestParser.intercept<D>(init),
      mergeMap((request) => fetch(request.toRequest())
          .then((response) => new HttpResponse(response, {
            acceptableStatuses: fetchInit.acceptableStatuses!,
            parseResponse: fetchInit.parseResponse,
          }))),
      this.responseInterceptors.intercept(init),
      this.responseParser.intercept(init),
      map((response) => response.data));
  }

  public static create(init: HttpClient.Init = {}) {
    const http = new HttpClient();
    http.addFetchDefaults(init);

    if (init.requestInterceptors) {
      http.requestInterceptors.add(init.requestInterceptors);
    }

    http.requestParser.add(HttpRequest.getRequestDataParser({
      ['application/json']: HttpRequest.parseJSONData,
      ['application/x-www-form-urlencoded']: HttpRequest.parseXwwwFormUrlEncodedData,
      ...init.requestParsers
    }));
    if (init.responseInterceptors) {
      http.responseInterceptors.add(init.responseInterceptors);
    }
    http.responseInterceptors.add(HttpResponse.getResponsStatusAssert(
      init.acceptableStatuses ||
      ((status: number) => status < HttpStatusCode.BAD_REQUEST)
    ));
    http.responseInterceptors.add(HttpResponse.getResponseRetryStrategy(
      init.retryAttempts,
      init.retryStrategy
    ));
    http.responseParser.add(HttpResponse.getResponseDataParser({
      ['application/json']: HttpResponse.parseToJson,
      ['application/pdf']: HttpResponse.parseToBlob,
      ['text/plain']: HttpResponse.parseToText,
      ['text/html']: HttpResponse.parseToText,
      ...init.responseParsers
    }));

    return http;
  }
}
