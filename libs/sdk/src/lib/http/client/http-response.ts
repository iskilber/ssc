import { Observable } from 'rxjs';
import {
  map,
  mergeMap,
  retry,
  retryWhen,
  tap
  } from 'rxjs/operators';
import {
  HttpContentType,
  HttpHeader,
  HttpStatusCode
  } from '../common';
import { HttpClient } from './http-client';
import { HttpInterceptor } from './http.interceptor';

export declare namespace HttpResponse {
  type BodyParser<D = any> = (response: Response) => Promise<D>;

  type AcceptableStatuses = ((status: number) => boolean) | HttpStatusCode[];

  interface Init<D> {
    acceptableStatuses: AcceptableStatuses;
    parseResponse?: HttpResponse.BodyParser<D>;
    data?: D;
  }
}

export class HttpResponse<D> {

  constructor(
    public response: Response, 
    private responseInit: HttpResponse.Init<D>
  ) {}

  public get acceptableStatuses(): HttpResponse.AcceptableStatuses {
    return this.responseInit.acceptableStatuses;
  }

  public get status() {
    return this.response ? this.response.status : undefined;
  }

  public get parseResponse(): (() => Promise<D>) | undefined {
    return this.responseInit.parseResponse && this.response ?
        () => this.responseInit.parseResponse!(this.response) : 
        undefined;
  }

  public get headers(): Headers | undefined {
    return this.response ? this.response.headers : undefined;
  }

  public get text(): Promise<string> {
    return this.response ? this.response.text() : Promise.resolve('');
  }

  public isStatusAccepted(
    defaultAcceptableStatuses?: HttpResponse.AcceptableStatuses
  ): boolean {
    const acceptableStatuses = 
        this.acceptableStatuses || defaultAcceptableStatuses;

    if (!this.response || !acceptableStatuses) {
      return true;
    } else {
      return Array.isArray(acceptableStatuses) ?
          acceptableStatuses.includes(this.response.status) :
          acceptableStatuses(this.response.status) as boolean;
    }
  }

  public get data(): D | undefined {
    return this.responseInit.data;
  }

  public clone(patch: Partial<HttpResponse.Init<D>> = {}) {
    return new HttpResponse(
      this.response,
      { ...this.responseInit, ...patch}
    );
  }

  public static parseToJson<D>(response: Response): Promise<D> {
    return response.json();
  }

  public static parseToBlob(response: Response): Promise<Blob> {
    return response.blob();
  }

  public static parseToText(response: Response): Promise<string> {
    return response.text();
  }

  public static parseToFormData(response: Response): Promise<FormData> {
    return response.formData();
  }

  public static getResponseDataParser<D>(
    parsers: {[contentType: string]: HttpResponse.BodyParser<D>}
  ): HttpInterceptor.ResponseInterceptor<D> {
    return (response$: Observable<HttpResponse<D>>) => {
      return response$.pipe(mergeMap((httpResponse: HttpResponse<D>) => {
        const contentType = HttpContentType.parse(
            httpResponse.headers!.get(HttpHeader.CONTENT_TYPE) ||
            'text/plain');
        let parserPromise: Promise<D>;
        if (httpResponse.parseResponse) {
          parserPromise = httpResponse.parseResponse();
        } else if (parsers[contentType.mediaType!]) {
          parserPromise = parsers[contentType.mediaType!](httpResponse.response);
        } else {
          parserPromise = httpResponse.text as any as Promise<D>;
        }
        return parserPromise.then((data: D) => httpResponse.clone({ data }));
      }));
    }  
  }

  public static getResponseRetryStrategy<D>(
    retryAttempts?: number,
    retryStrategy?: HttpClient.RetryStrategyFn
  ): HttpInterceptor.ResponseInterceptor<D> {
    return (response$: Observable<HttpResponse<D>>, init: HttpClient.FetchInit) => {
      if (init.retryAttempts) {
        return response$.pipe(retry(init.retryAttempts));
      } else if (init.retryStrategy) {
        return response$.pipe(retryWhen(init.retryStrategy));
      } else if (retryAttempts) {
        return response$.pipe(retry(retryAttempts));
      } else if (retryStrategy) {
        return response$.pipe(retryWhen(retryStrategy));
      } else {
        return response$;
      }
    }
  }

  public static getResponsStatusAssert<D>(
    acceptableStatus?: HttpResponse.AcceptableStatuses
  ): HttpInterceptor.ResponseInterceptor<D> {
    return (response$: Observable<HttpResponse<D>>) => {
      return response$.pipe(tap((httpResponse) => {
        if (!httpResponse.isStatusAccepted(acceptableStatus)) {
          throw httpResponse.response;
        }

        return httpResponse;
      }))
    }
  }

  public static getOnResponseStatusIntercetor<D>(
    statuses: HttpStatusCode | HttpStatusCode[],
    onStatusCaught: (response: Response) => void
  ): HttpInterceptor.ResponseInterceptor<D> {
    const httpStatuses = Array.isArray(statuses) ? statuses : [statuses];

    return (response$: Observable<HttpResponse<D>>) => {
      return response$.pipe(map((httpResponse) => {
        if (httpStatuses.includes(httpResponse.response.status)) {
          onStatusCaught(httpResponse.response);
        }
  
        return httpResponse;
      }));
    }
  }

  public static getOnUnauthorizedResponseInterceptor<D>(
    onStatusCaught: (response: Response) => void
  ): HttpInterceptor.ResponseInterceptor<D> {
    return HttpResponse.getOnResponseStatusIntercetor(
        HttpStatusCode.UNAUTHORIZED,
        onStatusCaught);
  }

  public static getLoggerInterceptor<D>(
    logResponse: (response: Response) => void
  ): HttpInterceptor.ResponseInterceptor<D> {
    return (response$: Observable<HttpResponse<D>>) => {
      return response$.pipe(map((httpResponse) => {
        logResponse(httpResponse.response);

        return httpResponse;
      }));
    }
  }
}