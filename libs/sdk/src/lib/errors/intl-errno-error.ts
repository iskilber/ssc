import {
  FormattedMessage,
  MessageValue
  } from 'react-intl';
import { Errno } from './errno-error';
import { ErrnoError } from './errno-error';

export interface IntlErrnoErrorMessageProps {
  values?: {[key: string]: MessageValue | JSX.Element};
  tagName?: string;
}

export interface IntlErrno {
  errno: Errno;
  titleIntl: FormattedMessage.MessageDescriptor;
  descriptionIntl?: FormattedMessage.MessageDescriptor;
}

export class IntlErrnoError extends ErrnoError {

  public static serialize(error: IntlErrnoError): string {
    return JSON.stringify({
      descriptionIntlProps: error.descriptionIntlProps,
      intlErrno: error.intlErrno,
      titleIntlProps: error.titleIntlProps,
    });
  }

  public static deserialize(error: string): IntlErrnoError {
    const {
      descriptionIntlProps,
      intlErrno,
      titleIntlProps
    } = JSON.parse(error);

    return new IntlErrnoError(intlErrno, titleIntlProps, descriptionIntlProps);
  }

  constructor(
      public readonly intlErrno: IntlErrno,
      public readonly titleIntlProps: IntlErrnoErrorMessageProps = {},
      public readonly descriptionIntlProps: IntlErrnoErrorMessageProps = {},
      origError?: any
  ) {
    super(
      intlErrno.errno,
      titleIntlProps ?
          titleIntlProps.values as {[index: string]: string | number} :
          undefined,
      origError
    );
  }

  public get titleIntl(): FormattedMessage.Props {
    const values = {
      ...this.params,
      ...this.titleIntlProps.values || {}
    };

    return {
        ...this.intlErrno.titleIntl,
        ...this.titleIntlProps,
        values
    } as FormattedMessage.Props;
  }

  public get descriptionIntl(): FormattedMessage.Props | undefined {
    const values = {
      ...this.params,
      ...this.descriptionIntlProps.values || {}
    };

    return this.intlErrno.descriptionIntl && {
      ...this.intlErrno.descriptionIntl,
      ...this.descriptionIntlProps,
      values
    } as FormattedMessage.Props;
  }

  /** @see ErrnoError#captureStackTrace */
  protected captureStackTrace() {
    /* tslint:disable no-string-literal */
    if (Error['captureStackTrace']) {
      Error['captureStackTrace'](this, IntlErrnoError);
    }
    /* tslint:disable enable */
  }
}
