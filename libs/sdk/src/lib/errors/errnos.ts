import { Errno } from './errno-error';
import { IntlErrno } from './intl-errno-error';

export const ERR_UNKNOWN: Errno = {
  code: 'ERR-0001',
  errno: 'ERR_UNKNOWN',
  message: 'Unknown error',
};

export const ERR_INTL_UNKNOWN: IntlErrno = {
  descriptionIntl: { id: 'errors.errUnknown.description' },
  errno: ERR_UNKNOWN,
  titleIntl: { id: 'errors.errUnknown.title' },
};
