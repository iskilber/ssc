export interface Errno {
  errno: string;
  code: string;
  message: string;
}

/**
 * Extended error by errno and code.
 *
 * @property Errno:
 */
export class ErrnoError extends Error {

  protected static formatMessage(
      messageTpl: string,
      tplArgs?: {[index: string]: string | number}
  ): string {
    return tplArgs ?
        Object.keys(tplArgs).reduce(
            (message, arg) =>
                message.replace(new RegExp(`{${arg}}`, 'g'), '' + tplArgs[arg]),
            messageTpl) :
        messageTpl;
  }

  public params?: {[index: string]: string | number};

  constructor(
      protected readonly _errno: Errno,
      msgTplArgs?: {[index: string]: string | number},
      public readonly origError?: any
  ) {
    super(ErrnoError.formatMessage(_errno.message, {
      ...msgTplArgs,
      code: _errno.code,
      errno: _errno.errno
    }));

    this.captureStackTrace();

    this.params = {
      ...msgTplArgs,
      code: _errno.code,
      errno: _errno.errno
    };
  }

  public get code(): string {
    return this._errno.code;
  }

  public get errno(): string {
    return this._errno.errno;
  }

  public stringify(stack?: string) {
    const data: any = {
      code: this.code,
      errno: this.errno,
      message: this.message,
      params: this.params
    };

    if (stack) {
      data.stack = stack;
    }

    return JSON.stringify(data);
  }

  /**
   * Maintains proper stack trace for where our error was thrown
   * (only available on V8)
   */
  protected captureStackTrace() {
    /* tslint:disable no-string-literal */
    if (Error['captureStackTrace']) {
      Error['captureStackTrace'](this, ErrnoError);
    }
    /* tslint:disable enable */
  }
}
