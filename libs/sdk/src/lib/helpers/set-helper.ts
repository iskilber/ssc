export abstract class SetHelper {

  public static toArray<I>(set: Set<I>): I[] {
    const retVal: I[] = [];
    set.forEach((i) => retVal.push(i));
    return retVal;
  }

  public static reduce<A, I>(
    set: Set<I>,
    reducer: (acc: A, item: I) => A,
    acc: A
  ): A {
    let retVal = acc;

    set.forEach((item) => retVal = reducer(retVal, item));

    return retVal;
  }
}
