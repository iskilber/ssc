export abstract class ReflectionHelper {
  public static isSymbol(value: any): boolean {
    /* tslint:disable triple-equals */
    return typeof value == 'symbol';
    /* tslint:enable */
  }

  public static isObject(value: any): boolean {
    const type = typeof value;
    return type === 'object' && value != null || type === 'function';
  }

  public static isFunction(value: any): boolean {
    /* tslint:disable triple-equals */
    return typeof value == 'function';
    /* tslint:enable */
  }

  public static isString(value: any): boolean {
    /* tslint:disable triple-equals */
    return typeof value == 'string';
    /* tslint:enable */
  }

  public static isNumber(value: any): boolean {
    /* tslint:disable triple-equals */
    return typeof value == 'number';
    /* tslint:enable */
  }

  public static set(obj: any, propName: string, value: any, defValue?: any) {
    const propValue = value !== undefined ? value : defValue;

    if (propValue !== undefined) {
      obj[propName] = value;
    }
    return obj;
  }

  public static removeUndefinedProps(obj: any) {
    return Reflect.ownKeys(obj)
        .filter((propName) => obj[propName] !== undefined)
        .reduce((acc, propName) => {
          acc[propName] = obj[propName];
          return acc;
        }, {});
  }

  public static excludeProps<T>(obj: any, propNames: string | string[]): T {
    const exclude = Array.isArray(propNames) ? propNames : [propNames];

    return Reflect.ownKeys(obj)
        .filter((propName: string) => !exclude.includes(propName))
        .reduce((acc, propName) => {
          acc[propName] = obj[propName];
          return acc;
        }, {}) as T;
  }

  public static isEmpty(val: any): boolean {
    return val === undefined || Number.isNaN(val) ||
      val === false || val === '' ||
      (Array.isArray(val) && val.length === 0) ||
      (val instanceof Map && val.size === 0) ||
      (val instanceof Set && val.size === 0) ||
      (ReflectionHelper.isObject(val) && Reflect.ownKeys(val).length === 0);
  }
}
