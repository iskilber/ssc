import { Errno } from '../errors';

export const ERR_POLLING_CLOSED: Errno = {
  code: 'APP-BROWSER-0001',
  errno: 'ERR_POLLING_CLOSED',
  message: 'Polling {pollingId} already closed!'
};

export const ERR_DATASOURCE_POLLING_FAILED: Errno = {
  code: 'STORE-0003',
  errno: 'ERR_DATASOURCE_POLLING_FAILED',
  message: 'Datasource polling {pollingId} failed on loop no ({loopNo})'
};
