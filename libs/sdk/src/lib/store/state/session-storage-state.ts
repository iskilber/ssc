import { ApplicationState } from './application-state';

export declare namespace SessionStorageState {
  interface Config {
    storageKey: string;
  }
}

export class SessionStorageState extends ApplicationState {
  constructor(private config: SessionStorageState.Config) {
    super();

    sessionStorage.setItem(config.storageKey, JSON.stringify({}));
  }

  public getState() {
    return JSON.parse(sessionStorage.getItem(this.config.storageKey)!);
  }

  public setState(state: ApplicationState.State) {
    sessionStorage.setItem(this.config.storageKey, JSON.stringify(state));
  }
}
