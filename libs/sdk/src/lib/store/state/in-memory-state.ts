import { ApplicationState } from './application-state';

export class InMemoryState extends ApplicationState {
  private state = {};

  public getState() {
    return this.state;
  }

  public setState(state: ApplicationState.State) {
    this.state = state;
  }
}
