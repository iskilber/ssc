import { ApplicationState } from './application-state';

export declare namespace LocalStorageState {
  interface Config {
    storageKey: string;
  }
}

export class LocalStorageState extends ApplicationState {
  constructor(private config: LocalStorageState.Config) {
    super();

    localStorage.setItem(config.storageKey, JSON.stringify({}));
  }

  public getState() {
    return JSON.parse(localStorage.getItem(this.config.storageKey)!);
  }

  public setState(state: ApplicationState.State) {
    localStorage.setItem(this.config.storageKey, JSON.stringify(state));
  }
}
