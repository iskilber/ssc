import {
  Observable,
  Subject
  } from 'rxjs';
import { share } from 'rxjs/operators';
import { Store } from './store';

export declare namespace ObservableStore {
  interface Constructor<S extends ObservableStore<T>, T> {
    new: (...args: any[]) => S;
  }

  interface ChangeEvent<S> {
    prevState: S | undefined;
    nextState: S | undefined;
  }

  type CreateChangeEvent<S, C extends ChangeEvent<S> = ChangeEvent<S>> = 
      (event: ChangeEvent<S>) => C;
}

export abstract class ObservableStore<
  S, C extends ObservableStore.ChangeEvent<S> = ObservableStore.ChangeEvent<S>
> extends Store<S> {

  protected state$ = new Subject<C>();

  public change: Observable<C> = this.state$.pipe(share());

  public setState(
    nextState: S, 
    createChangeEvent?: ObservableStore.CreateChangeEvent<S, C>
  ) {
    const prevState = this.getState();
    const changeEvent = createChangeEvent ?
      createChangeEvent({ prevState, nextState }) :
      { prevState, nextState };

    super.setState(nextState);

    this.state$.next(changeEvent as C);
  }

  public clear() {
    const prevState = this.getState();
    super.clear();
    
    this.state$.next({ prevState, nextState: undefined } as C);
  }
}
