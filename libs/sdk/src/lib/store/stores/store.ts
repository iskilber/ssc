import { ApplicationState } from '../state';

export class Store<S> {
  constructor(
    public readonly namespace: string,
    protected appState: ApplicationState,
    protected initState: S
  ) {
    if (!this.appState.has(this.namespace)) {
      this.appState.set(this.namespace, this.initState);
    }
  }

  public update(update: (state: S) => S) {
    const state = this.getState();

    this.setState(update(state));
  }

  public getState(): S {
    return this.appState.get<S>(this.namespace)!;
  }

  public setState(state: S) {
    this.appState.set<S>(this.namespace, state);
  }

  public reset() {
    this.setState(this.initState);
  }

  public clear() {
    this.appState.delete(this.namespace);
  }
}