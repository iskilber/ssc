import { FactoryProvider } from '../di';
import {
  ApplicationState,
  InMemoryState,
  LocalStorageState,
  SessionStorageState
  } from './state';

/*tslint:disable max-classes-per-file*/

export class InMemoryStateProvider extends FactoryProvider<InMemoryState> {
  constructor() { super(ApplicationState, () => new InMemoryState()); }
}

export class LocalStorageStateProvider extends
  FactoryProvider<LocalStorageState> {
    constructor(config: LocalStorageState.Config) {
      super(ApplicationState, () => new LocalStorageState(config));
    }
  }

export class SessionStorageStateProvider extends
  FactoryProvider<SessionStorageState> {
    constructor(config: SessionStorageState.Config) {
      super(ApplicationState, () => new SessionStorageState(config));
    }
  }
