import {
  Observable,
  Subscription
  } from 'rxjs';

/**
 * Datasource is class wrapping a store to take responsability
 * about data loading and changing.
 */
export abstract class Datasource<D> {
  protected subscriptions = new Set<Subscription>();

  protected abstract change: Observable<D | undefined>;

  public abstract setData(data: D): void;

  public abstract getData(): Observable<D>;

  public abstract getState(): D | undefined;

  /**
   * Fetches catalog data from server and store them into store.
   * 
   * May be usefull for any store observers.
   */
  public loadData() {
    this.getData().subscribe(
      (data) => this.setData(data),
      (err) => {/* @TODO: what with fetch errors */});
  }

  public subscribe<T = D>(
    observer: (data?: T) => void, 
    ...pipes: Array<(input: any) => Observable<any>>
  ): Subscription {
    const pipesArr = pipes || [];
    const subscription = (pipesArr.length > 0) ?
        Reflect.apply(this.change.pipe, this.change, pipes).subscribe(observer) :
        this.change.subscribe(observer as any as (data?: D) => void);

    subscription.add(() => {
      this.subscriptions.delete(subscription);

      if (this.subscriptions.size === 0) {
        this.tearDown();
      }
    });

    this.subscriptions.add(subscription);

    return subscription;
  }

  public tearDown() {
    /* NOOP */
  }
}