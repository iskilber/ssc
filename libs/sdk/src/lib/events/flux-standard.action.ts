import {
  AnyEvent,
  EventType
  } from './any-event.interface';

/** @see https://github.com/acdlite/flux-standard-action */
export abstract class FluxStandardAction<
  T extends EventType,
  P = undefined,
  M = undefined
> implements AnyEvent {

  public type: T;
  public payload?: P;
  public meta?: M;
  public error?: any;

  public static create<
    T extends EventType,
    P = undefined,
    M = undefined
  >(type: T, payload?: P, meta?: M): FluxStandardAction<T, P, M> {
    const action: FluxStandardAction<T, P, M> = { type };

    if (payload !== undefined) {
      action.payload = payload;
    }
    if (meta !== undefined) {
      action.meta = meta;
    }

    return action;
  }
}

export interface FluxStandardActionDefinition {
  ACTION_TYPE: EventType;
}
