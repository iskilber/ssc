import {
  AnyEvent,
  EventType
  } from './any-event.interface';
import { FluxStandardAction } from './flux-standard.action';

export abstract class FluxErrorAction<
  T extends EventType,
  P extends Error,
  M = undefined
> implements AnyEvent {

  public type: T;
  public error: boolean;
  public payload: P;
  public meta?: M;

  public static create<
    T extends EventType,
    P extends Error,
    M = undefined
  >(type: T, payload: P, meta?: M): FluxErrorAction<T, P, M> {
    const action = FluxStandardAction.create<T, P, M>(type, payload, meta);
    action.error = true;
    return action as FluxErrorAction<T, P, M>;
  }
}
