import {
  NextObserver,
  Observable,
  OperatorFunction,
  Subject,
  Subscription
  } from 'rxjs';
import { share } from 'rxjs/operators';
import { AnyEvent } from './any-event.interface';
import { EventsObservable } from './events-observable';

export declare namespace EventBus {
  type EventMiddleware<T extends AnyEvent> =
      (actions$: EventsObservable<T>) => Observable<T>;
  type EventHandler<T extends AnyEvent> =
      (actions$: EventsObservable<T>) => Subscription;
}

export class EventBus {

  private actionsSubject = new Subject<AnyEvent>();

  private events$ = this.actionsSubject.pipe(share());

  public dispatch(action: any) {
   this.actionsSubject.next(action);
  }

  public addEventMiddleware(
    callback: EventBus.EventMiddleware<any>
  ) {
    const actionsSubject = new Subject<any>();
    const effect$ = callback(new EventsObservable(actionsSubject));

    this.events$.subscribe(actionsSubject as NextObserver<any>);
    effect$.subscribe(this.actionsSubject as NextObserver<any>);
  }

  public addEventHandler<R = void>(
    callback: EventBus.EventHandler<any>
  ): Subscription {
    return callback(new EventsObservable<any>(this.events$));
  }

  public observe(source: Observable<AnyEvent>) {
    source.subscribe((event) => this.dispatch(event));
  }

  public subscribe(listener: any) {
    return this.events$.subscribe(listener);
  }

  public pipe(
    ...operators: Array<OperatorFunction<any, any>>
  ): Observable<any> {
    return Reflect.apply(this.events$.pipe, this.events$, operators);
  }
}
