export type EventType = string;

export interface AnyEvent {
  type: EventType;
}
