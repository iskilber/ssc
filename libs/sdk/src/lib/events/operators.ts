import { filter } from 'rxjs/operators';
import {
  FluxStandardAction,
  FluxStandardActionDefinition
  } from './flux-standard.action';

export function actionOf(...actionDefs: FluxStandardActionDefinition[]) {
  const actionTypes = actionDefs.map((actionDefs) => actionDefs.ACTION_TYPE);

  return filter(
      (action: FluxStandardAction<any, any, any>) => 
          actionTypes.includes(action.type));
}
