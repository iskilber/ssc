import {
  Observable,
  Operator
  } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AnyEvent } from './any-event.interface';

export class EventsObservable<T extends AnyEvent> extends Observable<T> {

  constructor(source$: Observable<T>) {
    super();
    this.source = source$;
  }

  public lift<R>(operator: Operator<T, R>) {
    const observable = new EventsObservable(this as any);
    observable.operator = operator as any;
    return observable as any;
  }

  public ofType<R extends T = T>(
    ...keys: Array<R['type']>
  ): EventsObservable<R> {
    return this.pipe(filter(({ type }) => {
      const length = keys.length;
      if (length === 1) {
        return type === keys[0];
      } else {
        for (let i = 0; i < length; i++) {
          if (type === keys[i]) {
            return true;
          }
        }
        return false;
      }
    })) as EventsObservable<R>;
  }
}
