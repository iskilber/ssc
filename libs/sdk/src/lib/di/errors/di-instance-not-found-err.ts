import { ReflectionHelper } from '../../helpers';

export class DiInstanceNotFoundErr extends TypeError {
  constructor(type: any, depsChain?: any[]) {
    super(
      `DI: instance of "${DiInstanceNotFoundErr.getDepName(type)}" ` +
      `not found in injector!` +
      `\n ${depsChain.map((dep) => DiInstanceNotFoundErr.getDepName(dep))}`);
  }

  private static getDepName = (dep: any) => {
    if (ReflectionHelper.isObject(dep)) {
      if (dep.name) {
        return dep.name;
      } else if (dep.constructor) {
        return dep.constructor.name;
      } else {
        return dep;
      }
    } else if (ReflectionHelper.isSymbol(dep)) {
      return dep.toString();
    } else {
      return dep;
    }
  }
}
