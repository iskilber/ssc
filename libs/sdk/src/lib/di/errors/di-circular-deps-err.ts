import {
  ReflectionHelper,
  SetHelper
  } from '../../helpers';

export class DiCircularDependencyError extends TypeError {
  constructor(type: any, depsLock: Set<any>) {
    super(
      `DI: Circular dependency occurred! ` +
      `${DiCircularDependencyError.getDepName(type)} -> ` +
      `"${DiCircularDependencyError.getDepsChain(depsLock)}"`
    );
  }

  private static getDepsChain(depsLock: Set<any>) {
    return SetHelper.toArray(depsLock)
      .map(DiCircularDependencyError.getDepName)
      .join(' -> ');
  }

  private static getDepName = (dep: any) => {
    if (ReflectionHelper.isObject(dep)) {
      if (dep.name) {
        return dep.name;
      } else if (dep.constructor) {
        return dep.constructor.name;
      } else {
        return dep;
      }
    } else if (typeof dep === 'symbol') {
      return dep.toString();
    } else {
      return dep;
    }
  };
}
