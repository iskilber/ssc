import { Injector } from '../injector';
import { Provider } from './provider';

export declare namespace FactoryProvider {
  type FactoryFunction<T> = (...args: any[]) => T;
}

export class FactoryProvider<T> extends Provider<T> {
  public static SINGLETON = true;

  public static NOT_SINGLETON = false;

  private result: T;

  constructor(
    type: any,
    private factoryFn: (...args: any[]) => T,
    private depsTypes: any[] = [],
    private singleton = FactoryProvider.SINGLETON
  ) {
    super(type);
  }

  public provide(injector: Injector) {
    if (!this.result || !this.singleton) {
      const deps = this.depsTypes.map((depType) => injector.inject(depType));

      this.result = Reflect.apply(this.factoryFn, undefined, deps);
    }
    return this.result;
  }
}
