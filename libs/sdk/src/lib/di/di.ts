import { Injector } from './injector';
import { Provider } from './providers';

export declare namespace DI {
  interface Init {
    providers?: Array<Provider<any>>;
  }
}

/* tslint:disable */
export class DI {

  public static spawnRootInjector() {
    const injector = new Injector();

    return injector;
  }
}
