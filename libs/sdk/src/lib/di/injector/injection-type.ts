import { InjectionToken } from './injection-token';

export type InjectionType = string | symbol | InjectionToken<any>;
