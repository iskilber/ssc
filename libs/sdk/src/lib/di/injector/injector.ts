import {
  ReflectionHelper,
  SetHelper
  } from '../../helpers';
import { CURRENT_INJECTOR } from '../common-tokens';
import {
  DiCircularDependencyError,
  DiInstanceNotFoundErr
  } from '../errors';
import {
  FactoryProvider,
  Provider,
  ValueProvider
  } from '../providers';
import { InjectionType } from './injection-type';

export class Injector {
  public static FLAG_OPTIONAL = 1;

  private providers: Map<InjectionType, Provider<any>> = new Map();

  private depsLock: Set<InjectionType> = new Set();

  private configs: Map<any, Array<Provider.Config<any>>> = new Map();

  constructor(private parent?: Injector) {}

  public use = (_provider: Provider<any> | Array<Provider<any>>): this => {
    const providers = Array.isArray(_provider) ? _provider : [_provider];

    for (const provider of providers) {
      if (this.configs.has(provider.injectionType)) {
        this.configs.get(provider.injectionType)!
            .forEach((configFn) => provider.configure(configFn));
      }
      this.providers.set(provider.injectionType, provider);
    }

    return this;
  }

  public value = (type: any, value: any) => {
    this.use(new ValueProvider(type, value));
  }

  public factory = (
    type: any,
    factoryFn: (...args: any[]) => any,
    depsTypes?: any[]
  ) => this.use(new FactoryProvider(type, factoryFn, depsTypes))

  public spawnChild() {
    return new Injector(this);
  }

  public configure(type: any, configFn: Provider.Config<any>) {
    if (!this.hasProvider(type)) {
      if (!this.configs.has(type)) {
        this.configs.set(type, []);
      }
      this.configs.get(type)!.push(configFn);
    } else {
      const provider = this.getProvider(type)!;

      provider.configure(configFn);
    }

    return this;
  }

  public hasProvider(type: any): boolean {
    return this.providers.has(type) ||
      (!!this.parent && this.parent.hasProvider(type));
  }

  public getProvider(type: any): Provider<any> | null {
    if (this.providers.has(type)) {
      return this.providers.get(type) || null;
    } else if (this.parent) {
      return this.parent.getProvider(type);
    } else {
      return null;
    }
  }

  public initialize(tokens: any | any[], flag: number = -1) {
    if (Array.isArray(tokens)) {
      tokens.forEach((token) => this.inject(token, flag));
    } else {
      this.inject(tokens, flag);
    }
  }

  public resolve<V = void>(resolveFn: (...args: any[]) => V, depTypes: any[]) {
    const deps = depTypes.map((depType) => this.inject(depType));

    return Reflect.apply(resolveFn, null, deps);
  }

  public injectList(tokens: any[]) {
    const deps = tokens.map((token: any) => this.inject(token));

    return deps;
  }

  public inject<T>(token: any, flag: number = -1): T {
    if (this.depsLock.has(token)) {
      throw new DiCircularDependencyError(token, this.depsLock);
    }
    this.depsLock.add(token);
    let dependency: any;

    if (token === CURRENT_INJECTOR) {
      dependency = this;
    } else if (this.providers.has(token)) {
      try {
        const provider = this.providers.get(token)!;
        dependency = provider!.provide(this);
        if (dependency === undefined) {
          console.warn(
            `Token "${Injector.getDepName(token)}" returned "undefined"! Is that expected?`);
        }
      } catch (error) {
        this.depsLock.clear();
        throw error;
      }
    } else if (this.parent) {
      try {
        dependency = this.parent.inject(token);
      } catch (error) {
        this.depsLock.clear();
        throw error;
      }
    } else {
      const depsChain = SetHelper.toArray(this.depsLock);

      this.depsLock.clear();
      if (flag === Injector.FLAG_OPTIONAL) {
        return undefined as any;
      }
      throw new DiInstanceNotFoundErr(token, depsChain);
    }
    this.depsLock.clear();
    return dependency;
  }

  public static getDepName = (dep: any) => {
    if (ReflectionHelper.isObject(dep)) {
      if (dep.name) {
        return dep.name;
      } else if (dep.constructor) {
        return dep.constructor.name;
      } else {
        return dep;
      }
    } else if (typeof dep === 'symbol') {
      return dep.toString();
    } else {
      return dep;
    }
  };
}
