export abstract class InjectionToken<T> {
  public value: T;
}
