import { getGreeting } from '../support/app.po';

describe('sentencer-browser', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to sentencer-browser!');
  });
});
