import {
  SentencerResultMessage,
  useSentencerIsDoneState
  } from 'apps/sentencer-browser/src/sentencer';
import * as React from 'react';
import {
  InfoOutputMessage,
  OUTPUT_MESSAGE_TYPES,
  useOutputMessagesState
  } from '../../output';
import { ConsoleInputLine } from '../console-input-line.component/console-input-line.component';
import { ConsoleLine } from '../console-line.component/console-line.component';
import { ConsoleText } from '../console-text.component/console-text.component';

const styles = require('./console.screen.component.scss');

export declare namespace ConsoleScreen {
  interface Props {
    className?: string;
  }

  type Component = React.FunctionComponent<Props>;
}

const COMPONENT_NAME = 'ConsoleScreen';

export const ConsoleScreen: ConsoleScreen.Component = (props) => {
  const outputMessages = useOutputMessagesState();
  const isDone = useSentencerIsDoneState();

  return (
    <>
    <article className={styles.console}>
      <section className={styles.wrapper}>
        <>
        {outputMessages.map((record) => {
          switch (record.message.type) {
            case OUTPUT_MESSAGE_TYPES.INFO:
              return (
                <ConsoleLine key={record.messageId}>
                    <ConsoleText text={record.message.payload.message}/>
                </ConsoleLine>);
            case OUTPUT_MESSAGE_TYPES.ERROR:
              return (
                <ConsoleLine key={record.messageId}>
                    <ConsoleText 
                        text={record.message.payload.message} 
                        isError={true}/>
                </ConsoleLine>);
            case OUTPUT_MESSAGE_TYPES.RESPONSE:
              return (
                <ConsoleLine key={record.messageId}>
                  <span>
                    <ConsoleText text={record.message.payload.question} />
                    <ConsoleText text=": " />
                    <ConsoleText text={record.message.payload.response} />
                  </span>
                </ConsoleLine>);
            }
          }
        )}
        {!isDone ? <ConsoleInputLine /> : null }
        </>
      </section>
    </article>
    <SentencerResultMessage />
    </>
  )
}

ConsoleScreen.displayName = COMPONENT_NAME;
