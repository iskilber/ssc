import * as React from 'react';
import { ConsoleLineLabel } from '../console-line-label.component/console-line-label.component';

const styles = require('./console-line.component.scss');

export declare namespace ConsoleLine {
  interface Props {
    contentClassName?: string;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ConsoleLine: ConsoleLine.Component = (props) => {
  return (
    <p className={styles.consoleLine}>
      <span><ConsoleLineLabel /></span>
      <span className={props.contentClassName}>{props.children}</span>
    </p>
  );
}

ConsoleLine.displayName = 'ConsoleLine';