import * as React from 'react';
import { ConsoleText } from '../console-text.component/console-text.component';

export declare namespace ConsoleLineLabel {
  type Component = React.FunctionComponent;
}

export const ConsoleLineLabel: ConsoleLineLabel.Component = () => {
  return (
    <>
      <ConsoleText text={location.origin} />
      <ConsoleText text=":" />
      <ConsoleText text="Sentencer" />
      <ConsoleText text="$ " />
    </>
  );
}

ConsoleLineLabel.displayName = 'ConsoleLineLabel';