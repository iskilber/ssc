import * as React from 'react';
import { useSentencerQuestionState } from '../../../sentencer';
import { useInputBufferTextState } from '../../input';
import { ConsoleInputMarker } from '../console-input-marker.component/console-input-marker.component';
import { ConsoleLine } from '../console-line.component/console-line.component';
import { ConsoleText } from '../console-text.component/console-text.component';

const styles = require('./console-input-line.component.scss');

export declare namespace ConsoleInputLine {
  type Component = React.FunctionComponent;
}

export const ConsoleInputLine: ConsoleInputLine.Component = (props) => {
  const question = useSentencerQuestionState();
  const inputBufferText = useInputBufferTextState();

  return question ? (
    <ConsoleLine>
      <span className={styles.consoleInputLine}>
        <ConsoleText text={question.question}/>
        <ConsoleText text=": "/>
        <ConsoleText text={inputBufferText} />
        <ConsoleInputMarker />
      </span>
    </ConsoleLine>
  ) : null;
}

ConsoleInputLine.displayName = 'ConsoleInputLine';