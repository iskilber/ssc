import { IntlMessage } from '@ssc/ui-react';
import * as React from 'react';
const styles = require('./console-text.component.scss');

export declare namespace ConsoleText {
  interface Props {
    text: IntlMessage.Message;
    isError?: boolean;
  }

  type Component = React.FunctionComponent<Props>;
}

export const ConsoleText: ConsoleText.Component = (props) => {
  const classNames = [styles.consoleText];
  if (props.isError) {
    classNames.push(styles.error);
  }
  return (
    <span className={classNames.join(' ')}><IntlMessage message={props.text}/></span>
  )
}

ConsoleText.displayName = 'ConsoleText';

