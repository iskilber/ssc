import { IntlMessage } from '@ssc/ui-react';
import * as React from 'react';
import { useConsoleInputFeature } from '../input';
import { useConsoleOutputFeature } from '../output';
import { ConsoleScreen } from './console.screen.component/console.screen.component';

/**
 * Entry component for console feature module.
 */
export declare namespace Console {
  interface Props {
    initMessages: IntlMessage.Message[];
  }

  type Component = React.FunctionComponent<Props>;
}

export const Console: Console.Component = (props) => {
  // Initialize event handlers, stores and services related to
  // writing at the console output.
  useConsoleOutputFeature({ initMessages: props.initMessages });
  // Initialize event handlers. store and services related to
  // reading from console input.
  useConsoleInputFeature();

  return (
    <ConsoleScreen />
  );
}

Console.displayName = 'Console';
