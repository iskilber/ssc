import * as React from 'react';

const styles = require('./console-input-marker.component.scss');

export declare namespace ConsoleInputMarker {
  type Component = React.FunctionComponent;
}

export const ConsoleInputMarker: ConsoleInputMarker.Component = () => {
  return (
    <span className={styles.consoleMarker}></span>
  );
};

ConsoleInputMarker.displayName = 'ConsoleInputMarker';