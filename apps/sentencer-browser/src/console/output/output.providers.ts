import {
  ApplicationState,
  FactoryProvider
  } from '@ssc/sdk';
import { IntlMessage } from '@ssc/ui-react';
import { OutputMessageStore } from './outpt-message.store';
import { InfoOutputMessage } from './output-messages';

export const CONSOLE_OUTPUT_INIT_MESSAGES = Symbol('app.console.initOutputMessages');

const outputMessageStoreProvider = new FactoryProvider(
  OutputMessageStore,
  (appState: ApplicationState, initMessages: IntlMessage.Message[]) => 
      new OutputMessageStore(
        appState, 
        initMessages.map((message) => InfoOutputMessage.create({ message} ))),
  [ApplicationState, CONSOLE_OUTPUT_INIT_MESSAGES]
);

export const CONSOLE_OUTPUT_PROVIDERS = [
  outputMessageStoreProvider
];