import { FluxStandardAction } from '@ssc/sdk';
import { OutputMessage } from './output-messages';

export enum OUTPUT_EVENT_TYPES {
  MESSAGE = 'app.console.output.message'
}

/**
 * Ask console output feature to print a message on screen.
 */
export declare namespace OutputMessageAction {
  type ACTION_TYPE = OUTPUT_EVENT_TYPES.MESSAGE;
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
  type Payload = OutputMessage.Message;
}

export class OutputMessageAction {
  public static ACTION_TYPE: OUTPUT_EVENT_TYPES.MESSAGE = 
      OUTPUT_EVENT_TYPES.MESSAGE;

  public static create(
    payload: OutputMessageAction.Payload
  ): OutputMessageAction.Action {
    return FluxStandardAction.create(
      OutputMessageAction.ACTION_TYPE, payload);
  }
}