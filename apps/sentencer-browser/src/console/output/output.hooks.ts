import {
  actionOf,
  ValueProvider
  } from '@ssc/sdk';
import {
  IntlMessage,
  useEventHandler,
  useInject,
  useProvide,
  useSubscription
  } from '@ssc/ui-react';
import * as React from 'react';
import { map } from 'rxjs/operators';
import { OutputMessageStore } from './outpt-message.store';
import { OutputMessageAction } from './output.events';
import {
  CONSOLE_OUTPUT_INIT_MESSAGES,
  CONSOLE_OUTPUT_PROVIDERS
  } from './output.providers';

export function useOutputMessageStore(): OutputMessageStore {
  const { store } = useInject({ store: OutputMessageStore });

  return store;
}

export function useOutputMessagesState(): OutputMessageStore.StoredMessage[] {
  const outputMessageStore = useOutputMessageStore();

  const [messages, setMessages] = React.useState(outputMessageStore.getMessages());

  useSubscription(
      () => outputMessageStore.observeMessages()
          .subscribe((messages) => setMessages(messages)), 
      [outputMessageStore])

  return messages;
}

function useObserveOutputMessages() {
  const outputMessageStore = useOutputMessageStore();

  useEventHandler(
    (events$) => events$
      .pipe(
          actionOf(OutputMessageAction),
          map((action: OutputMessageAction.Action) => action.payload))
      .subscribe((message) => outputMessageStore.pushMessage(message)),
    [outputMessageStore])
}

export interface ConsoleOutputProps {
  initMessages: IntlMessage.Message[];
}

export function useConsoleOutputFeature(props: ConsoleOutputProps) {
  useProvide([
    new ValueProvider(CONSOLE_OUTPUT_INIT_MESSAGES, props.initMessages),
    ...CONSOLE_OUTPUT_PROVIDERS
  ]);

  useObserveOutputMessages();
}
