import { IntlMessage } from '@ssc/ui-react';

export enum OUTPUT_MESSAGE_TYPES {
  QUESTION = 'question',
  RESPONSE = 'response',
  INFO = 'info',
  ERROR = 'error'
}


export declare namespace OutputMessage {
  interface Message<T extends string = any, P = any> {
    type: T;
    payload?: P;
    meta?: { messageId: string }
  }
}

export class OutputMessage {
  public static create<T extends string, P = any>(type: T, payload?: P) {
    const message: OutputMessage.Message<T, P> = { type };

    if (payload) {
      message.payload = payload;
    }
    return message;
  }
}

export declare namespace InfoOutputMessage {
  type MESSAGE_TYPE = OUTPUT_MESSAGE_TYPES.INFO;
  type Message = OutputMessage.Message<MESSAGE_TYPE, Payload>;
  interface Payload {
    message: IntlMessage.Message
  }
}

export class InfoOutputMessage {
  public static MESSAGE_TYPE: OUTPUT_MESSAGE_TYPES.INFO = OUTPUT_MESSAGE_TYPES.INFO;
  public static create(payload: InfoOutputMessage.Payload) {
    return OutputMessage.create(InfoOutputMessage.MESSAGE_TYPE, payload);
  }
}

export declare namespace ErrorOutputMessage {
  type MESSAGE_TYPE = OUTPUT_MESSAGE_TYPES.ERROR;
  type Message = OutputMessage.Message<MESSAGE_TYPE, Payload>;
  interface Payload {
    message: IntlMessage.Message
  }
}

export class ErrorOutputMessage {
  public static MESSAGE_TYPE: OUTPUT_MESSAGE_TYPES.ERROR = OUTPUT_MESSAGE_TYPES.ERROR;
  public static create(payload: ErrorOutputMessage.Payload): ErrorOutputMessage.Message {
    return OutputMessage.create(ErrorOutputMessage.MESSAGE_TYPE, payload);
  }
}

export declare namespace ResponseOutputMessage {
  type MESSAGE_TYPE = OUTPUT_MESSAGE_TYPES.RESPONSE;
  type Message = OutputMessage.Message<MESSAGE_TYPE, Payload>;
  interface Payload {
    question: IntlMessage.Message,
    response: string
  }
}

export class ResponseOutputMessage {
  public static MESSAGE_TYPE: OUTPUT_MESSAGE_TYPES.RESPONSE = OUTPUT_MESSAGE_TYPES.RESPONSE;
  public static create(payload: ResponseOutputMessage.Payload): ResponseOutputMessage.Message {
    return OutputMessage.create(ResponseOutputMessage.MESSAGE_TYPE, payload);
  }
}

