import {
  ApplicationState,
  ObservableStore
  } from '@ssc/sdk';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';
import { OutputMessage } from './output-messages';

/**
 * Stores messages to be printend on screen.
 */
export declare namespace OutputMessageStore {

  interface StoredMessage {
    messageId: string;
    message: OutputMessage.Message;
  }

  interface State {
    messages: StoredMessage[];
    lastMessageId: string;
  }
}

export class OutputMessageStore extends ObservableStore<OutputMessageStore.State> {

  public static getMessageId(ix: number) {
    return `msg_${ix}`;
  }

  constructor(appState: ApplicationState, defaultMessages: OutputMessage.Message[]) {
    super('app.console.output.messages', appState, {
      lastMessageId: OutputMessageStore.getMessageId(defaultMessages.length -1),
      messages: defaultMessages.map((message, ix) => ({
        messageId: OutputMessageStore.getMessageId(ix),
        message
      }))
    });
  }

  public getMessages(): OutputMessageStore.StoredMessage[] {
    return this.getState().messages || [];
  }

  public pushMessage(message: OutputMessage.Message) {
    const prevState = this.getState();
    const messageId = OutputMessageStore.getMessageId(prevState.messages.length);
    const newMessage: OutputMessageStore.StoredMessage = { message, messageId };

    const nextState = {
      ...prevState,
      lastMessageId: messageId,
      messages: [
        ...prevState.messages,
        newMessage
      ],
    };
    this.setState(nextState);
  }

  public getLastMessage(): OutputMessageStore.StoredMessage | undefined {
    const lastMessageId = this.getState().lastMessageId;
    const messages = this.getState().messages;

    return messages.find(
        (storedMessage) => storedMessage.messageId === lastMessageId);
  }

  public observeMessages(): Observable<OutputMessageStore.StoredMessage[]> {
    return this.change.pipe(
      filter((change) => change.prevState.messages !== change.nextState.messages),
      map((change) => change.nextState.messages));
  }
}