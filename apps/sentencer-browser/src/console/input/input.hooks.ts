import { actionOf } from '@ssc/sdk';
import {
  useEventBus,
  useEventHandler,
  useInject,
  useProvide,
  useRefByCallback,
  useStopper,
  useSubscription
  } from '@ssc/ui-react';
import * as React from 'react';
import {
  fromEvent,
  Observable
  } from 'rxjs';
import { InputBufferStore } from './input-buffer.store';
import {
  InputDeleteEvent,
  InputEndEvent,
  InputExitEvent,
  InputInputEvent,
  InputSubmitEvent
  } from './input.events';
import { INPUT_PROVIDERS } from './input.providers';
import {
  filter,
  } from 'rxjs/operators';

function useKeyboardInput() {
  const eventBus = useEventBus();
  const inputBufferStore = useInputBufferStore();
  const stopper = useStopper();

  const keyPress$ = useRefByCallback<Observable<KeyboardEvent>>(() => 
      fromEvent<KeyboardEvent>(document, 'keypress')
          .pipe(stopper.takeUntil()));

  const keyDown$ = useRefByCallback<Observable<KeyboardEvent>>(() => 
      fromEvent<KeyboardEvent>(document, 'keydown')
          .pipe(stopper.takeUntil()));

  useSubscription(
    () => keyPress$.current
      .pipe(filter((event) => event.ctrlKey && event.key === 'c'))
      .subscribe(() => eventBus.dispatch(InputExitEvent.create())),
    []);

  useSubscription(
    () => keyPress$.current
      .pipe(filter((event) => event.key === 'Enter'))
      .subscribe(() => eventBus.dispatch(InputSubmitEvent.create({
        input: inputBufferStore.bufferToString()
      }))),
    []);
  
  useSubscription(
    () => keyPress$.current
      .pipe(filter((event) => event.key !== 'Enter' && !event.ctrlKey))
      .subscribe((event) => eventBus.dispatch(InputInputEvent.create({
        char: event.key
      }))),
    []);

  useSubscription(
    () => keyDown$.current
      .pipe(filter((event) => event.key === 'Backspace'))
      .subscribe(() => eventBus.dispatch(InputDeleteEvent.create())),
    []);

  useEventHandler(
      (events$) => events$
        .pipe(actionOf(InputEndEvent))
        .subscribe(() => stopper.stop()),
      [stopper]);
}

function useInputBufferStore(): InputBufferStore {
  const { input } = useInject({ input: InputBufferStore });

  return input;
}

/**
 * Observes console input events and write into input buffer.
 */
function useInputBufferEffect() {
  const inputBufferStore = useInputBufferStore();

  const reducer = React.useCallback(
    (action: InputDeleteEvent.Action | InputInputEvent.Action) => {
      switch (action.type) {
        case InputDeleteEvent.ACTION_TYPE:
            inputBufferStore.removeChar();
            break;
        case InputInputEvent.ACTION_TYPE:
            inputBufferStore.addChar(action.payload.char)
            break;
      }
    }, 
    [inputBufferStore]);

  useEventHandler<InputDeleteEvent.Action | InputInputEvent.Action>(
      (events$) => events$
          .pipe(actionOf(InputDeleteEvent, InputInputEvent))
          .subscribe(reducer),
      [inputBufferStore]);

  useEventHandler<InputSubmitEvent.Action>(
    (events$) => events$
      .pipe(actionOf(InputSubmitEvent)) 
      .subscribe(() => inputBufferStore.clearBuffer()),
    [inputBufferStore])
}

export function useInputBufferTextState() {
  const inputBufferStore = useInputBufferStore();
  const [text, setText] = React.useState(inputBufferStore.bufferToString());

  useSubscription(
    () => inputBufferStore
        .observeBuffer()
        .subscribe((text) => setText(text)),
    [setText, inputBufferStore]);

  return text;
}


export function useConsoleInputFeature() {
  useProvide(INPUT_PROVIDERS);
  useKeyboardInput();
  useInputBufferEffect();
}