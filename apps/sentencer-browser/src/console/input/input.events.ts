import { FluxStandardAction } from '@ssc/sdk';

export enum INPUT_EVENT_TYPES {
  EXIT = 'app.console.input.exit',
  INPUT = 'app.console.input.input',
  DELETE = 'app.console.input.delete',
  PASTE = 'app.console.input.paste',
  SUBMIT = 'app.console.input.submit',
  END = 'app.console.input.end'
}

export declare namespace InputExitEvent {
  type ACTION_TYPE = INPUT_EVENT_TYPES.EXIT;
  type Action = FluxStandardAction<ACTION_TYPE>;
}

export class InputExitEvent {
  public static ACTION_TYPE: INPUT_EVENT_TYPES.EXIT = 
      INPUT_EVENT_TYPES.EXIT;

  public static create(): InputExitEvent.Action {
    return FluxStandardAction.create(InputExitEvent.ACTION_TYPE);
  }
}

export declare namespace InputInputEvent {
  type ACTION_TYPE = INPUT_EVENT_TYPES.INPUT;
  interface Payload {
    char: string;
  }
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
}

export class InputInputEvent {
  public static ACTION_TYPE: INPUT_EVENT_TYPES.INPUT = 
      INPUT_EVENT_TYPES.INPUT;

  public static create(
    payload: InputInputEvent.Payload
  ): InputInputEvent.Action {
    return FluxStandardAction.create(
      InputInputEvent.ACTION_TYPE, payload);
  }
}

export declare namespace InputSubmitEvent {
  type ACTION_TYPE = INPUT_EVENT_TYPES.SUBMIT;
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
  interface Payload {
    input: string
  }
}

export class InputSubmitEvent {
  public static ACTION_TYPE: INPUT_EVENT_TYPES.SUBMIT = 
      INPUT_EVENT_TYPES.SUBMIT;

  public static create(payload: InputSubmitEvent.Payload): InputSubmitEvent.Action {
    return FluxStandardAction.create(InputSubmitEvent.ACTION_TYPE, payload);
  }
}

export declare namespace InputDeleteEvent {
  type ACTION_TYPE = INPUT_EVENT_TYPES.DELETE;
  type Action = FluxStandardAction<ACTION_TYPE>;
}

export class InputDeleteEvent {
  public static ACTION_TYPE: INPUT_EVENT_TYPES.DELETE = 
      INPUT_EVENT_TYPES.DELETE;

  public static create(): InputDeleteEvent.Action {
    return FluxStandardAction.create(
      InputDeleteEvent.ACTION_TYPE);
  }
}

export declare namespace InputEndEvent {
  type ACTION_TYPE = INPUT_EVENT_TYPES.END;
  type Action = FluxStandardAction<ACTION_TYPE>;
}

export class InputEndEvent {
  public static ACTION_TYPE: INPUT_EVENT_TYPES.END = 
      INPUT_EVENT_TYPES.END;

  public static create(): InputEndEvent.Action {
    return FluxStandardAction.create(
      InputEndEvent.ACTION_TYPE);
  }
}