import {
  ApplicationState,
  ObservableStore
  } from '@ssc/sdk';
import {
  filter,
  map
  } from 'rxjs/operators';

export declare namespace InputBufferStore {
  interface State {
    buffer: string[];
  }
}

export class InputBufferStore extends ObservableStore<InputBufferStore.State> {
  constructor(
    appState: ApplicationState
  ) {
    super('app.console.input.buffer', appState, { buffer: [] });
  }

  public addChar(char: string) {
    const prevState = this.getState();
    const newState = {
      ...prevState,
      buffer: [...prevState.buffer, char]
    }
    this.setState(newState);
  }

  public removeChar() {
    const prevState = this.getState();
    const newState = {
      ...prevState,
      buffer: prevState.buffer.slice(0, -1)
    }
    this.setState(newState);
  }

  public bufferToString(): string {
    return this.getState().buffer.join('');
  }

  public observeBuffer() {
    return this.change.pipe(
        filter((change) => change.prevState.buffer !== change.nextState.buffer),
        map((change) => change.nextState.buffer.join('')));
  }

  public clearBuffer() {
    const prevState = this.getState();
    const newState = {
      ...prevState,
      buffer: []
    };
    this.setState(newState);
  }
}