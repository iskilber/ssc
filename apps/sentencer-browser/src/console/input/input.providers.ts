import {
  ApplicationState,
  FactoryProvider
  } from '@ssc/sdk';
import { InputBufferStore } from './input-buffer.store';

const inputBufferStoreProvider = new FactoryProvider(
    InputBufferStore,
    (appState: ApplicationState) => new InputBufferStore(appState),
    [ApplicationState]);

export const INPUT_PROVIDERS = [
  inputBufferStoreProvider
];
