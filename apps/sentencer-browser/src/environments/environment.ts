// This file can be replaced during build by using the `fileReplacements` array.
// When building for production, this file is replaced with `environment.prod.ts`.

const LOCALES = ['en', 'fr'];
const THEMES = [ 'dark-theme', 'light-theme' ];

export const environment = {
  production: false,
  configUrls: [
    '/assets/config.base.json',
    '/assets/config.{env}.json'
  ],
  locales: LOCALES,
  defaultLocale: 'en',
  defaultThemeName: THEMES[0],
  themeNames: THEMES,
  initMessages: [
    'Sentencer v0.0.1',
    '==============================================================================',
    { id: 'app.console.wellcome' },
    { id: 'app.console.wellcomeNext' }
  ],
  responsePattern: '{question.who} {question.what} {question.when} {question.where}',
  initQuestions: [
    { 
      question: { id: 'app.console.confirmQuestion' },
      questionId: 'question.confirm.start'
    },
    {  
      question: { 
        id: 'app.console.languageQuestion', 
        values: { languages: LOCALES.join('/')} 
      },
      questionId: 'question.language'
    },
    { 
      question: { 
        id: 'app.console.themeQuestion',
        values: { themes: THEMES.join('/')}
      },
      questionId: 'question.theme'
    },
    {  
      question: { id: 'app.console.whoQuestion' },
      questionId: 'question.who'
    },
    {  
      question: { id: 'app.console.whatQuestion' },
      questionId: 'question.what'
    },
    { 
      question: { id: 'app.console.whenQuestion' },
      questionId: 'question.when'
    },
    { 
      question: { id: 'app.console.whereQuestion' },
      questionId: 'question.where'
    },
    { 
      question: { id: 'app.console.confirmSentence' },
      questionId: 'question.sentenceConfirm'
    },
  ]
};
