import {
  ApplicationState,
  FactoryProvider
  } from '@ssc/sdk';
import {
  SentencerQuestionStore,
  SentencerResponseStore,
  SentencerResultStore
  } from './store';

export const SENTENCER_INIT_QUESTION_TOKEN = Symbol('app.sentencer.questions');

export const SENTENCER_RESPONSE_PATTERN = Symbol('app.sentencer.responsePattern');

const sentencerResponsesStoreProvider = new FactoryProvider(
    SentencerResponseStore,
    (appState: ApplicationState, responsePattern: string) => new SentencerResponseStore(
        appState, responsePattern),
    [ApplicationState, SENTENCER_RESPONSE_PATTERN]);

const sentencerQuestionStoreProvider = new FactoryProvider(
    SentencerQuestionStore,
    (appState: ApplicationState, questions: SentencerQuestionStore.Question[]) => 
        new SentencerQuestionStore(appState, questions),
    [ApplicationState, SENTENCER_INIT_QUESTION_TOKEN]);

const sentencerResultStoreProvider = new FactoryProvider(
    SentencerResultStore,
    (appState: ApplicationState, responseStore: SentencerResponseStore) => 
        new SentencerResultStore(appState, responseStore),
    [ApplicationState, SentencerResponseStore]);


export const SENTENCER_PROVIDERS = [
  sentencerResponsesStoreProvider,
  sentencerQuestionStoreProvider,
  sentencerResultStoreProvider
];
