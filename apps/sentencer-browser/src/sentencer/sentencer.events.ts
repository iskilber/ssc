import { FluxStandardAction } from '@ssc/sdk';

export enum SENTENCER_EVENT_TYPES {
  RESPONSE = 'app.sentencer.response',
  NEXT = 'app.sentencer.next',
  DONE = 'app.sentencer.done'
}

export declare namespace SentencerResponseAction {
  type ACTION_TYPE = SENTENCER_EVENT_TYPES.RESPONSE;
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
  interface Payload {
    questionId: string,
    response: string
  }
}

export class SentencerResponseAction {
  public static ACTION_TYPE: SentencerResponseAction.ACTION_TYPE = 
      SENTENCER_EVENT_TYPES.RESPONSE;

  public static create(
    payload: SentencerResponseAction.Payload
  ): SentencerResponseAction.Action {
    return FluxStandardAction.create(SentencerResponseAction.ACTION_TYPE, payload);
  }
}

export declare namespace SentencerNextAction {
  type ACTION_TYPE = SENTENCER_EVENT_TYPES.NEXT;
  type Action = FluxStandardAction<ACTION_TYPE, Payload>;
  interface Payload { questionId: string }
}

export class SentencerNextAction {
  public static ACTION_TYPE: SentencerNextAction.ACTION_TYPE = 
      SENTENCER_EVENT_TYPES.NEXT;

  public static create(
    payload: SentencerNextAction.Payload
  ): SentencerNextAction.Action {
    return FluxStandardAction.create(SentencerNextAction.ACTION_TYPE, payload);
  }
}

export declare namespace SentencerDoneAction {
  type ACTION_TYPE = SENTENCER_EVENT_TYPES.DONE;
  type Action = FluxStandardAction<ACTION_TYPE>;
}

export class SentencerDoneAction {
  public static ACTION_TYPE: SentencerDoneAction.ACTION_TYPE = 
      SENTENCER_EVENT_TYPES.DONE;

  public static create(): SentencerDoneAction.Action {
    return FluxStandardAction.create(SentencerDoneAction.ACTION_TYPE);
  }
}