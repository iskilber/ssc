import {
  ApplicationState,
  ObservableStore
  } from '@ssc/sdk';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';
import { SentencerResponseStore } from './sentencer-responses.store';

export declare namespace SentencerResultStore {
  interface State {
    isDone: boolean;
  }
}

export class SentencerResultStore extends ObservableStore<SentencerResultStore.State>{
  constructor(
    appState: ApplicationState, 
    private responsesStore: SentencerResponseStore
  ) {
    super('app.sentencer.result', appState, { isDone: false });
  }

  public isDone() {
    return this.getState().isDone;
  }

  public setDone() {
    const prevState = this.getState();
    const newState = {
      ...prevState,
      isDone: true
    };
    this.setState(newState);
  }

  public observeIsDone(): Observable<boolean> {
    return this.change.pipe(
      filter((change) => change.prevState.isDone !== change.nextState.isDone),
      map((change) => change.nextState.isDone));
  }

  public getResultMessage(): string {
    return this.responsesStore.getResultMessage();
  }
}