import {
  ApplicationState,
  ObservableStore
  } from '@ssc/sdk';
import { IntlMessage } from '@ssc/ui-react';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';

export declare namespace SentencerQuestionStore {
  interface Question {
    questionId: string, question: IntlMessage.Message
  }

  interface State {
    questions: Question[];
    currentQuestionId: string;
  }
}

export class SentencerQuestionStore extends ObservableStore<SentencerQuestionStore.State> {
  constructor(appState: ApplicationState, initQuestion: SentencerQuestionStore.Question[]) {
    super(
      'app.sentencer.questions', 
      appState, 
      { questions: initQuestion, currentQuestionId: 'question.confirm.start' });
  }

  public getCurrentQuestion(): SentencerQuestionStore.Question | undefined {
    const currentQuestionId = this.getState().currentQuestionId;

    return this.getState().questions
        .find((question) => question.questionId === currentQuestionId)
  }

  public observeCurrentQuestion(): Observable<SentencerQuestionStore.Question> {
    return this.change.pipe(
      filter((change) => 
          change.prevState.currentQuestionId !== change.nextState.currentQuestionId),
      map(() => this.getCurrentQuestion()));
  }

  public setNextQuestion(questionId: string) {
    const prevState = this.getState();
    const newState = {
      ...prevState,
      currentQuestionId: questionId
    };
    this.setState(newState);
  }
}