import {
  ApplicationState,
  ObservableStore
  } from '@ssc/sdk';

export declare namespace SentencerResponseStore {
  interface State {
    responses: {
      [questionId: string]: string;
    },
    resultMessagePattern: string;
  }
}

export class SentencerResponseStore extends ObservableStore<SentencerResponseStore.State> {
  constructor(appState: ApplicationState, resultMessagePattern: string) {
    super('app.sentencer.response', appState, {
      resultMessagePattern,
      responses: {}
    });
  }

  public setResponse(questionId: string, response: string) {
    const prevState = this.getState();
    const newState = {
      ...prevState,
      responses: {
        ...prevState.responses,
        [questionId]: response
      }
    };
    this.setState(newState);
  }

  public getResponse(questionId: string): string | undefined {
    return this.getState().responses[questionId];
  }

  public getTemplatePattern(): string {
    return this.getState().resultMessagePattern;
  }

  public getResponses(): { [questionId: string]: string; } {
    return this.getState().responses;
  }

  public getResultMessage(): string {
    return Reflect
        .ownKeys(this.getResponses())
        .reduce<string>(
          (template: string, questionId: string) => 
              template.replace(
                  `{${questionId}}`, 
                  this.getResponse(questionId)), 
        this.getTemplatePattern());
  }
}