import {
  actionOf,
  AnyEvent,
  ValueProvider
  } from '@ssc/sdk';
import {
  ThemeChangeAction,
  useEventHandler,
  useEventMiddleware,
  useInject,
  useProvide,
  useSubscription
  } from '@ssc/ui-react';
import { IntlSwitchAction } from 'libs/ui-react/src/lib/intl/intl.events';
import * as React from 'react';
import {
  from,
  Observable,
  of
  } from 'rxjs';
import {
  map,
  switchMap
  } from 'rxjs/operators';
import {
  useLanguagNames,
  useThemesNames
  } from '../app/app.hooks';
import {
  InputEndEvent,
  InputSubmitEvent
  } from '../console/input/input.events';
import {
  ErrorOutputMessage,
  ResponseOutputMessage
  } from '../console/output';
import { OutputMessageAction } from '../console/output/output.events';
import {
  SentencerDoneAction,
  SentencerNextAction,
  SentencerResponseAction
  } from './sentencer.events';
import {
  SENTENCER_INIT_QUESTION_TOKEN,
  SENTENCER_PROVIDERS,
  SENTENCER_RESPONSE_PATTERN
  } from './sentencer.providers';
import {
  SentencerQuestionStore,
  SentencerResponseStore,
  SentencerResultStore
  } from './store';

/**
 * Inject SentencerResponseStore
 */
export function useSentencerResponsesStore(): SentencerResponseStore {
  const { store } = useInject({ store: SentencerResponseStore });

  return store;
}

export function useSentencerQuestionStore(): SentencerQuestionStore {
  const { store } = useInject({ store: SentencerQuestionStore });

  return store;
}


export function useSentencerResultStore(): SentencerResultStore {
  const { store } = useInject({ store: SentencerResultStore });

  return store;
}

/**
 * @private
 * 
 * Private effect: Listen for question responses and store them into store
 */
function useListenResponses() {
  const store = useSentencerResponsesStore();

  useEventHandler(
      (actions$: Observable<SentencerResponseAction.Action>) => actions$
          .pipe(
              actionOf(SentencerResponseAction),
              map((action: SentencerResponseAction.Action) => action.payload))
          .subscribe((payload) => 
              store.setResponse(payload.questionId, payload.response)));
}


export function useSentencerQuestionState(): SentencerQuestionStore.Question | undefined {
  const store = useSentencerQuestionStore();

  const [question, setQuestion] = React.useState(store.getCurrentQuestion());

  useSubscription(
      () => store
        .observeCurrentQuestion()
        .subscribe((question) => setQuestion(question)),
      [store, setQuestion]);

  return question;
}

function useHandleStartConfirmQuestion() {
  const sentencerQuestionStore = useSentencerQuestionStore();

  return React.useCallback((response: string) => {
    if (['y', 'm'].includes(response.toLocaleLowerCase())) {
      const question = sentencerQuestionStore.getCurrentQuestion();
      return from([
        OutputMessageAction.create(ResponseOutputMessage.create({
          question: question.question,
          response
        })),
        SentencerResponseAction.create({
          questionId: question.questionId,
          response
        }),
        SentencerNextAction.create({ questionId: 'question.language' })
      ]);
    } else {
      return of(OutputMessageAction.create(ErrorOutputMessage.create({
        message: { id: 'app.console.confirmQuestion.error' }
      })));
    }
  }, [sentencerQuestionStore]);
}

function useHandleLanguageQuestion() {
  const sentencerQuestionStore = useSentencerQuestionStore();
  const locales = useLanguagNames();

  return React.useCallback((response: string) => {
    const question = sentencerQuestionStore.getCurrentQuestion();

    if (!response || locales.includes(response.toLocaleLowerCase())) {
      const actions: AnyEvent[] = [
        OutputMessageAction.create(ResponseOutputMessage.create({
          question: question.question,
          response
        })),
        SentencerResponseAction.create({
          questionId: question.questionId,
          response
        }),
        SentencerNextAction.create({ questionId: 'question.theme' })
      ];
      if (response) {
        actions.push(IntlSwitchAction.create({ locale: response }));
      }
      return from(actions);
    } else {
      return of(OutputMessageAction.create(ErrorOutputMessage.create({
        message: { 
          id: 'app.console.languageQuestion.error',
          values: {
            languages: locales.join(', ')
          }
        }
      })));
    }
  }, [sentencerQuestionStore]);
}


function useHandleThemeQuestion() {
  const sentencerQuestionStore = useSentencerQuestionStore();
  const themes = useThemesNames();

  return React.useCallback((response: string) => {
    const question = sentencerQuestionStore.getCurrentQuestion();

    if (!response || themes.includes(response.toLocaleLowerCase())) {
      const actions: AnyEvent[] = [
        OutputMessageAction.create(ResponseOutputMessage.create({
          question: question.question,
          response
        })),
        SentencerResponseAction.create({
          questionId: question.questionId,
          response
        }),
        SentencerNextAction.create({ questionId: 'question.who' })
      ];
      if (response) {
        actions.push(ThemeChangeAction.create({ themeName: response }));
      }
      return from(actions);
    } else {
      return of(OutputMessageAction.create(ErrorOutputMessage.create({
        message: { 
          id: 'app.console.themeQuestion.error',
          values: {
            themes: themes.join(', ')
          }
        }
      })));
    }
  }, [sentencerQuestionStore]);
}

function useSentenceBuilderQuestion() {
  return React.useCallback((
    question: SentencerQuestionStore.Question,
    response: string, 
    nextQuestionId: string
  ) => {
    return from([
      OutputMessageAction.create(ResponseOutputMessage.create({
        question: question.question,
        response
      })),
      SentencerResponseAction.create({
        questionId: question.questionId,
        response
      }),
      SentencerNextAction.create({ questionId: nextQuestionId })
    ]);
  }, []);
}

function useBooleanQuestion() {
  return React.useCallback((
    question: SentencerQuestionStore.Question,
    response: string,
    nextAcceptQuestionId?: string,
    nextRejectQuestionId?: string,
    nextAcceptAction?: AnyEvent
  ) => {
    const boolResponse = response.toLocaleLowerCase() === 'y';

    if (['y', 'n'].includes(response.toLocaleLowerCase())) {
      const actions: AnyEvent[] = [
        OutputMessageAction.create(ResponseOutputMessage.create({
          question: question.question,
          response
        })),
        SentencerResponseAction.create({
          questionId: question.questionId,
          response
        })
      ];

      if (nextAcceptQuestionId && boolResponse) {
        actions.push(SentencerNextAction.create({ 
          questionId: nextAcceptQuestionId
        }));
      } 
      if (nextRejectQuestionId && !boolResponse) {
        actions.push(SentencerNextAction.create({ 
          questionId: nextRejectQuestionId
        }));
      }
      if (nextAcceptAction && boolResponse) {
        actions.push(nextAcceptAction);
      }

      return from(actions);
    } else {
      return of(OutputMessageAction.create(ErrorOutputMessage.create({
        message: { id: 'app.console.confirmQuestion.error' }
      })));
    }
  }, []);
}

function useSentencerResponseHandler() {
  const sentencerQuestionStore = useSentencerQuestionStore();

  const handleStartConfirmQuestion = useHandleStartConfirmQuestion();
  const handleLanguageQuestion = useHandleLanguageQuestion();
  const handleThemeQuestion = useHandleThemeQuestion();
  const handleSentenceBuilderQuestion = useSentenceBuilderQuestion();
  const handleBoolResponse = useBooleanQuestion();

  useEventMiddleware((actions$) => actions$.pipe(
    actionOf(InputSubmitEvent),
    map((action: InputSubmitEvent.Action) => action.payload.input),
    switchMap((response) => {
      const currentQuestion = sentencerQuestionStore.getCurrentQuestion();

      switch (currentQuestion.questionId) {
        case 'question.confirm.start':
          return (handleStartConfirmQuestion(response));
        case 'question.language':
          return (handleLanguageQuestion(response));
        case 'question.theme':
          return (handleThemeQuestion(response));
        case 'question.who':
          return handleSentenceBuilderQuestion(
              currentQuestion, response, 'question.what');
        case 'question.what':
          return handleSentenceBuilderQuestion(
              currentQuestion, response, 'question.when');
        case 'question.when':
            return handleSentenceBuilderQuestion(
                currentQuestion, response, 'question.where');
        case 'question.where':
          return handleSentenceBuilderQuestion(
              currentQuestion, response, 'question.sentenceConfirm');
        case 'question.sentenceConfirm':
          return handleBoolResponse(
              currentQuestion, response, null, 'question.who', 
              SentencerDoneAction.create());
      }
    })
  ), [ sentencerQuestionStore ]);
}

function useSentencerNextHandler() {
  const sentencerQuestionStore = useSentencerQuestionStore();

  useEventHandler(
    (actions$) => actions$
      .pipe(
          actionOf(SentencerNextAction),
          map((action: SentencerNextAction.Action) => action.payload.questionId))
      .subscribe((questionId) => 
          sentencerQuestionStore.setNextQuestion(questionId)), 
    [ sentencerQuestionStore ]);
}

function useObserveSentencerDone() {
  const sentencerResultStore = useSentencerResultStore();

  useEventHandler(
    (actions$) => actions$
      .pipe(actionOf(SentencerDoneAction))
      .subscribe(() => sentencerResultStore.setDone()), 
    [ sentencerResultStore ]);
}

export function useSentencerIsDoneState() {
  const sentencerResultStore = useSentencerResultStore();
  const [isDone, setIsDone] = React.useState(sentencerResultStore.isDone());

  useSubscription(
    () => sentencerResultStore
        .observeIsDone()
        .subscribe((isDone) => setIsDone(isDone)),
    [sentencerResultStore, isDone]);

  return isDone;
}

function useInputEndOnDone() {
  useEventMiddleware(
    (actions$) => actions$.pipe(
      actionOf(SentencerDoneAction),
      map(() => InputEndEvent.create())
    ), 
    []);
}

export interface SentencerFeatureModuleProps {
  questions: SentencerQuestionStore.Question[];
  responsePattern: string;
}

/**
 * Setup Sentencer feature module: 
 * 
 * provides store and services and setup event handling.
 */
export function useSentencerFeatureModule(props: SentencerFeatureModuleProps) {
  useProvide([
    new ValueProvider(SENTENCER_INIT_QUESTION_TOKEN, props.questions),
    new ValueProvider(SENTENCER_RESPONSE_PATTERN, props.responsePattern),
    ...SENTENCER_PROVIDERS
  ]);

  useListenResponses();
  useSentencerResponseHandler();
  useSentencerNextHandler();
  useObserveSentencerDone();
  useInputEndOnDone();
}