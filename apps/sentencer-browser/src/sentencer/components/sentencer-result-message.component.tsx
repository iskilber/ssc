import * as React from 'react';
import {
  useSentencerIsDoneState,
  useSentencerResponsesStore
  } from '../sentencer.hooks';
const styles = require('./sentencer-result-message.component.scss');

export declare namespace SentencerResultMessage {
  type Componet = React.FunctionComponent;
}

export const SentencerResultMessage: SentencerResultMessage.Componet = () => {
  const isDone = useSentencerIsDoneState();
  const sentencerResponseStore = useSentencerResponsesStore();

  return isDone ? (
    <div className={styles.sentencerResultLayer}>
      <div className={styles.sentencerResultBox}>
        <p className={styles.sentencerResultMessage}>
        {sentencerResponseStore.getResultMessage()}
        </p>
      </div>
    </div>
  ) : null;
}

SentencerResultMessage.displayName = 'SentencerResultMessage';
