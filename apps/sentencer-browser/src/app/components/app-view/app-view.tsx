import {
  IntlDynamicProvider,
  useDependencyInitialize,
  useEventLogging,
  useExposeAppState,
  useIntlFeature,
  useThemeFeature,
  useThemeSwitchObserver
  } from '@ssc/ui-react';
import * as React from 'react';
import '../../../assets/themes/base.theme.scss';
import '../../../assets/themes/dark.theme.scss';
import '../../../assets/themes/light.theme.scss';
import { Console } from '../../../console';
import { useSentencerFeatureModule } from '../../../sentencer';
import {
  useAppEnvConfig,
  useBootstrapState
  } from '../../app.hooks';
import './app-view.reset.global.scss';


const styles = require('./app-view.component.scss');

export declare namespace AppView {
  interface Props {
    servicesOnStartUp: any[];
  }

  type Component = React.FunctionComponent<Props>;
}

export const AppView: AppView.Component = (props) => {
  const appEnvConfig = useAppEnvConfig();
  useIntlFeature();
  useThemeFeature();
  // Make available application global state via window object from
  // browser console for debug purposes.
  useExposeAppState('ssc_app_state');
  useEventLogging();
  // Initialize those services in order to listen and track state changes.
  useDependencyInitialize(props.servicesOnStartUp);
  useThemeSwitchObserver();

  useSentencerFeatureModule({ 
    questions: appEnvConfig.initQuestions,
    responsePattern: appEnvConfig.responsePattern
  });
  
  const isBootstrapDone = useBootstrapState();

  return (
    <IntlDynamicProvider>
      <div className={styles.wrapper}>
      { isBootstrapDone ? (
        <Console initMessages={appEnvConfig.initMessages}/>) : (
        <div>Loading...</div>)}
      </div>
    </IntlDynamicProvider>
  )
}

AppView.displayName = 'AppView';
