import { ApplicationState } from '@ssc/sdk';
import {
  InjectorProvider,
  IntlStore,
  ThemeStore
  } from '@ssc/ui-react';
import * as React from 'react';
import { APP_PROVIDERS } from '../providers';
import { AppView } from './app-view/app-view';

const PROVIDERS = [ ...APP_PROVIDERS ];

const SERVICES_ON_STARTUP = [
  ApplicationState,
  IntlStore,
  ThemeStore
]

export const App = () => {
  return (
    <InjectorProvider providers={PROVIDERS}>
      <AppView servicesOnStartUp={SERVICES_ON_STARTUP}/>
    </InjectorProvider>
  ); 
};
