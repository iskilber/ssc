import {
  APP_CONFIG_ENV_VALUES,
  APP_ENV_TOKEN,
  ApplicationState,
  EventBus,
  FactoryProvider,
  HttpClient,
  HttpHeader,
  HttpRequest,
  InMemoryStateProvider,
  Provider,
  SDK_CONFIG_PROVIDERS,
  ValueProvider
  } from '@ssc/sdk';
import {
  IntlMessage,
  IntlStore,
  ThemeStore,
  UI_THEME_INIT_TOKEN,
  UI_THEME_PROVIDERS
  } from '@ssc/ui-react';
import { addLocaleData } from 'react-intl';
import { environment } from '../environments/environment';

const eventBusProvider = new FactoryProvider(EventBus, () => new EventBus());

const httpClientProvider = new FactoryProvider(
  HttpClient, 
  () => {
    const http = HttpClient.create({
      base: location.href,
      requestInterceptors: [
        HttpRequest.getRequestHeaderDecorator(
            HttpHeader.CONTENT_TYPE, 'application/json')
      ]
    });

    return http;
  });
  
const appEnvProvider = new ValueProvider(
    APP_ENV_TOKEN, 
    environment.production ? 'prod' : 'dev');

const appStateProvider = new InMemoryStateProvider();

const appConfigCompiledEnvs = new ValueProvider(APP_CONFIG_ENV_VALUES, environment);

const intlStoreProvider = new FactoryProvider(
  IntlStore,
  (appState: ApplicationState, env: any) => {
    const store = new IntlStore(appState, {
      defaultLocale: env.defaultLocale,
      currentLocale: env.defaultLocale,
      enabledLocales: env.locales,
      messages: env.locales.reduce((acc, locale) => {
        acc[locale] = require(`../assets/i18n/messages.${locale}.yaml`);
  
        return acc;
      }, {}),
    });

    addLocaleData(env.locales.reduce(
      (acc, locale) => 
          [...acc, ...require(`react-intl/locale-data/${locale}`)], 
      []));
    
    return store;
  },
  [ApplicationState, APP_CONFIG_ENV_VALUES]
);

const themeInitProvider = new ValueProvider<ThemeStore.State>(
  UI_THEME_INIT_TOKEN,
  { 
    currentThemeName: environment.defaultThemeName,
    defaultThemeName: environment.defaultThemeName,
    themeNames: environment.themeNames,
  }
);


export const APP_PROVIDERS: Provider<any>[] = [
  appEnvProvider,
  eventBusProvider,
  httpClientProvider,
  appStateProvider,
  appConfigCompiledEnvs,
  intlStoreProvider,
  themeInitProvider,
  ...SDK_CONFIG_PROVIDERS,
  ...UI_THEME_PROVIDERS
]