import { APP_CONFIG_ENV_VALUES } from '@ssc/sdk';
import {
  useConfig,
  useInject
  } from '@ssc/ui-react';
import * as React from 'react';

export function useAppEnvConfig(): any {
  const { config } = useInject({ config: APP_CONFIG_ENV_VALUES });

  return config;
}

export function useLanguagNames(): string[] {
  const { config } = useInject({ config: APP_CONFIG_ENV_VALUES });

  return config.locales;
}

export function useThemesNames(): string[] {
  const { config } = useInject({ config: APP_CONFIG_ENV_VALUES });

  return config.themeNames;
}

export function useBootstrapState() {
  const [isBootstrapDone, setBootstrapState] = React.useState<boolean>(false);
  const { appEnvSettings } = useInject({ appEnvSettings: APP_CONFIG_ENV_VALUES });
  const config = useConfig();

  React.useEffect(() => {
    config.load(appEnvSettings.configUrls)
        .then(() => setBootstrapState(true));
  }, [setBootstrapState]);


  return isBootstrapDone;
}