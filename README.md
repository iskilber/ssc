# SSC


## Concept

Hi.

Example is done in bit more complex way in order to play a bit with application
architecture chanlenges.

Repository is organized as monorepo. Simple command `npx nx dep-graph` will
show you how packages are related to each other. This monorepo does not 
contains any backend apps or libraries. (But it might)

There is one application package `sentencer-browser` to compose and run 
example application. The main app package is supported by two libraries: `sdk`
and `ui-react`. First is framework agnostic and delivers basic capabilities.
Second provide shared helper functions and componentes for application ui.

### Debug & Inspect

In order to see all passing events, please open console in dev tools.

In order to inspect current application global state, please search in
console for object `window.ssc_app_state.state`.

### Known issues

The example app is just a draft. So there are those known issues:

 - not supported copy & paste
 - not complete light theme
 - not complete french dictionary
 - not implemented `ctrl+c` support
 - not implemented `N` reaction on first confirm question
 - not really used runtime configuration, which would be better as yaml file.

### Sentencer-browser

Application package with running example app. It is `React/RxJs/Flux` application.
The `React/Redux` version will be delivered soon in `Sentencer-redux-browser`.

Ui layer is connected with stores, services and event handlers via hooks.

Application is composed from two modules: 

 - `sentencer` takes care about handling questions and harvesting responses
 - `console` renders simple console ui to print out messages and observe user
            input. There are two subpackages: `output` and `input`

### Sdk

Provides:
  - runtime configuration service
  - runtime dependency injection management
  - events handling by simple event bus
  - http client
  - observable store layer

### Ui-react

Provides:
  - intl components, hooks, services based on `React-Intl`
  - simple theme management
  - custom share helper hooks, mostly to handle observable subscriptions,
    and services injection.

## Get started

Install dependencies. Make sure your node and npm is up to date.

Install might require global `yarn` command on your environment. It is a
bug in `Nx` tools.

## Resources

- [Nx](https://nx.dev)
- [Rx spec](http://reactivex.io/)
- [RxJS](https://www.learnrxjs.io/)

